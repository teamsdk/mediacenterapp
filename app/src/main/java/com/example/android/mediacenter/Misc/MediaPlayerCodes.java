package com.example.android.mediacenter.Misc;

/**
 * Enum used to define possible Playback Codes for our media player.
 *
 * @author TeamSDK
 * @since November 24 2015
 * @version November 24 2015
 */
public enum MediaPlayerCodes {

    PLAYLIST_COMMAND,
    PLAYBACK_COMMAND,
    CURRENT_PLAYBACK,
    PLAYLIST_SETTING

} // end enum
