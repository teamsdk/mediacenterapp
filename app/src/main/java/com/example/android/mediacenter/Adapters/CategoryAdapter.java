package com.example.android.mediacenter.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import shared.CategoryDescriptor;
import com.example.android.mediacenter.R;

import java.util.List;

/**
 *  The following class is the adapter used in the Video Library for category view. The adapter
 *  controls the setup of the list of categories being displayed in the listview.
 *
 * @author  TeamSDK
 * @since   November 15 2015
 * @version November 15 2015
 */
public class CategoryAdapter extends ArrayAdapter<CategoryDescriptor> {

    /**
     * The constructor for CategoryAdapter used to setup the ListView.
     *
     * @param context - The current context.
     * @param resource - The resource ID for a layout file containing a list_view ot use when
     *                 instantiating views.
     * @param categories - The objects to represent in the ListView.
     *
     * @since   November 15 2015
     * @version November 15 2015
     */
    public CategoryAdapter(Context context, int resource, List<CategoryDescriptor> categories) {

        super(context, resource, categories); // use the constructor for ArrayAdapter.

    } // end constructor

    /**
     * The following method is used to get a View that display the data at the specified position in
     * the data set.
     *
     * @param position - The position of the item within the adapter's data set of the item whose
     *                 view we want.
     * @param convertView - The old view to reuse, if possible.
     * @param parent - The parent that this view will eventually be attached to.
     * @return A View corresponding to the data at the specified position.
     *
     * @since   November 15 2015
     * @version November 15 2015
     */
    @SuppressLint({"SetTextI18n", "InflateParams"})
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView; // use the old view

        if (v == null) { // if the old view is not available
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.category_list_item, null); // inflate from a XML layout file.
        }

        CategoryDescriptor category = getItem(position); // get the CategoryDescriptor at the current position.

        if (category != null) { // if our CategoryDescriptor is valid
            // retrieve the TextView placeholders within category_list_item to display category parameters
            TextView tt1 = (TextView) v.findViewById(R.id.category_name);

            if (tt1 != null) { // if our TextView is valid
                tt1.setText(category.getCategoryName()); // set the name of the category
            }
        }

        return v;

    } // end method

} // end class