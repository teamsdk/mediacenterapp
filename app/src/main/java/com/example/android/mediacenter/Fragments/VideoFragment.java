package com.example.android.mediacenter.Fragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import com.example.android.mediacenter.Adapters.VideoAdapter;
import shared.VideoDescriptor;
import com.example.android.mediacenter.Misc.MediaPlayerCodes;
import com.example.android.mediacenter.Misc.QueryCodes;
import com.example.android.mediacenter.Misc.RequestMediaPlayerFunction;
import com.example.android.mediacenter.Misc.RequestQuery;
import com.example.android.mediacenter.R;
import com.example.android.mediacenter.Activities.VideoPlayback;
import java.io.ObjectInputStream;
import java.util.LinkedList;

/**
 * The fragment to use when loading a videos view in the video library.
 *
 * @author TeamSDK
 * @since November 15 2015
 * @version November 28 2015
 */
public class VideoFragment extends Fragment
                           implements SwipeRefreshLayout.OnRefreshListener {

    private static final String DATA_STORAGE = "MyDataFile"; // store data required
    private static final String MEDIA_TYPE = "v";
    private static final MediaPlayerCodes MEDIA_PLAYER_COMMAND = MediaPlayerCodes.PLAYLIST_COMMAND;
    private static final int NEW_PLAYLIST_COMMAND = 0;
    private static final int VIDEO_PORT = 4004;

    private SwipeRefreshLayout swipeLayout; // our refreshing swipe layout we are using
    private VideoAdapter videoAdapter;
    private LinkedList<VideoDescriptor> listOfVideos = new LinkedList<>(); // our videos
    private String mediaCenterAddress;
    private QueryCodes query; // the type of query this fragment will issue
    private Boolean needsCategory; // if a database category field is required in this fragment
    private String category; // the database category if required

    /**
     * Called to have the fragment instantiate its user interface view.
     *
     * @param inflater - the layoutinflater object that can be used to inflate any views in the fragment
     * @param container - this is the parent view that the fragment's UI should be attached to
     * @param savedInstanceState - the previous state to re-construct this fragment from.
     * @return The view for the fragment's UI
     *
     * @since November 15 2015
     * @version November 28 2015
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_video, container, false); // get the view
        SharedPreferences settings = this.getActivity().getSharedPreferences(DATA_STORAGE, 0);
        Bundle bundle = getArguments();

        ListView listView = (ListView) view.findViewById(R.id.list_video_videos);

        swipeLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_videos_container);
        swipeLayout.setOnRefreshListener(this);

        // Restore currently selected device
        mediaCenterAddress = settings.getString("currentDeviceIP", "");

        // retrieve information needed in this fragment
        query = QueryCodes.values()[bundle.getInt("query")];
        needsCategory = bundle.getBoolean("needsCategory");

        if (needsCategory) {
            category = bundle.getString("category");
        }

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            /**
             * The following method handles the clicking of songs in the list of videos.
             *
             * @param parent - The AdapterView where the click happened.
             * @param view - The view within the AdapterView that was clicked.
             * @param position - The position of the view in the adapter.
             * @param id - The row id of the item that was clicked.
             * @return NONE
             *
             * @since November 15 2015
             * @version November 15 2015
             */
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                new SendStartPlaylist().execute(position);

            } // end method

        } // end anonymous class
        );

        videoAdapter = new VideoAdapter(getActivity().getBaseContext(), R.layout.video_list_item, listOfVideos);
        listView.setAdapter(videoAdapter);

        new GetVideosList().execute();

        return view;

    } // end method

    /**
     * This is the handler for pulling down for refresh.
     *
     * @return NONE
     *
     * @since November 15 2015
     * @version November 28 2015
     */
    @Override
    public void onRefresh() {

        listOfVideos.clear();
        videoAdapter.notifyDataSetChanged();
        new GetVideosList().execute();

    } // end method

    /**
     * This is contains the task to be run when a pull down for refresh is done and when entering
     * the fragment.
     *
     * @author TeamSDK
     * @since November 16 2015
     * @version November 28 2015
     */
    private class GetVideosList extends AsyncTask<Void, Void, Void> {

        /**
         * Following defines our background task to be done.
         *
         * @param params - the parameters of the task
         * @return A result.
         *
         * @since November 16 2015
         * @version November 28 2015
         */
        @SuppressWarnings("unchecked")
        @Override
        protected Void doInBackground(Void... params) {

            RequestQuery videos = new RequestQuery(mediaCenterAddress, VIDEO_PORT);

            if (needsCategory) { // query the media center for videos for a category
                videos.sendQueryRequest(query, category);
            }
            else { // query the media center for all videos
                videos.sendQueryRequest(query);
            }

            try {
                ObjectInputStream iStream = videos.receiveRequestedInformation();
                listOfVideos.addAll((LinkedList<VideoDescriptor>) iStream.readObject());
                iStream.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;

        } // end method

        /**
         * Runs on the UI thread after doInBackground finishes its task.
         *
         * @param result - the result of the operations computed by doInBackground
         * @return NONE
         *
         * @since November 16 2015
         * @version November 28 2015
         */
        @Override
        protected void onPostExecute(Void result) {

            videoAdapter.notifyDataSetChanged();
            swipeLayout.setRefreshing(false);
            super.onPostExecute(result);

        } // end method

    } // end class

    /**
     * This is contains the task to be run when a video is selected.
     *
     * @author TeamSDK
     * @since November 25 2015
     * @version November 27 2015
     */
    private class SendStartPlaylist extends AsyncTask<Integer, Void, Void> {

        /**
         * Following defines our background task to be done for the refresh and when entering
         * the fragment.
         *
         * @param params - the parameters of the task
         * @return A result.
         *
         * @since November 25 2015
         * @version November 27 2015
         */
        @Override
        protected Void doInBackground(Integer... params) {

            RequestMediaPlayerFunction startVideo = new RequestMediaPlayerFunction(mediaCenterAddress);

            String playlist = new String();
            VideoDescriptor video = listOfVideos.get(params[0]);
            int videoID = video.getId();

            for (VideoDescriptor s : listOfVideos) {
                playlist = playlist + s.getId();

                    if (s.getId() != listOfVideos.getLast().getId()) {
                        playlist = playlist + "$";
                    }

                }

            startVideo.sendStartPlaybackRequest(MEDIA_PLAYER_COMMAND, NEW_PLAYLIST_COMMAND, MEDIA_TYPE, playlist, videoID, "");
            startVideo.closeConnection();
            return null;

        } // end method

        /**
         * Runs on the UI thread after doInBackground finishes its task.
         *
         * @param result - the result of the operations computed by doInBackground
         * @return NONE
         *
         * @since November 25 2015
         * @version November 27 2015
         */
        @Override
        protected void onPostExecute(Void result) {

            Intent intent = new Intent(getActivity(), VideoPlayback.class);
            startActivity(intent);
            super.onPostExecute(result);

        } // end method

    } // end class

} // end class