package com.example.android.mediacenter.Activities;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import com.example.android.mediacenter.Fragments.VideoFragment;
import com.example.android.mediacenter.Misc.QueryCodes;
import com.example.android.mediacenter.Misc.ViewPagerAdapter;
import com.example.android.mediacenter.R;

/**
 * Our activity to be used when the user selects a category from the CATEGORIES tab in Video
 * Library.
 *
 * @author TeamSDK
 * @since November 19 2015
 * @version November 28 2015
 */
public class CategoryActivity extends AppCompatActivity {

    private String category; // to hold the database category of the category upon receiving it from previous activity

    /**
     * This is the method invoked during the creation of this activity.
     *
     * @param savedInstanceState - The saved state this activity will be able to restore itself to
     *                           under special circumstances.
     * @return NONE
     *
     * @since November 19 2015
     * @version November 28 2015
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);

        Bundle extras = getIntent().getExtras();
        Toolbar toolbar = (Toolbar) findViewById(R.id.category_toolbar); // get toolbar
        ViewPager viewPager = (ViewPager) findViewById(R.id.category_viewpager);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.category_tabs);
        Button buttonNowPlaying = (Button) findViewById(R.id.now_playing_button_video);

        category = extras.getString("categoryName"); // retrieve the database category of the category
        setTitle(category); // here we set the title of the activity

        // set toolbar functionality here
        setSupportActionBar(toolbar);
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);

        buttonNowPlaying.setOnClickListener(new View.OnClickListener() {

            /**
             * The following method determines the action when the 'Now Playing' button is clicked.
             *
             * @param view - the view that was clicked
             * @return NONE
             *
             * @since November 28 2015
             * @version November 28 2015
             */
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(CategoryActivity.this, VideoPlayback.class);
                startActivity(intent);

            } // end method

        } // end anonymous class
        );

    } // end method

    /**
     * Sets up items on the ViewPager for user's view.
     *
     * @param viewPager - the ViewPager to display on
     * @return NONE
     *
     * @since November 19 2015
     * @version November 19 2015
     */
    private void setupViewPager(ViewPager viewPager) {

        Bundle videoFragBundle = new Bundle(); // bundle used for our SongFragment
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        Fragment videoFragment = new VideoFragment();

        // information to pass to our VideoFragment
        videoFragBundle.putInt("query", QueryCodes.VIDEOS_FOR_CATEGORY_QUERY.ordinal());
        videoFragBundle.putBoolean("needsCategory", true);
        videoFragBundle.putString("category", category);

        // give Fragments the information they need
        videoFragment.setArguments(videoFragBundle);

        adapter.addFragment(videoFragment, "VIDEOS");
        viewPager.setAdapter(adapter);

    } // end method

} // end class