package com.example.android.mediacenter.Fragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import com.example.android.mediacenter.Adapters.SongAdapter;
import shared.SongDescriptor;
import com.example.android.mediacenter.Misc.MediaPlayerCodes;
import com.example.android.mediacenter.Misc.QueryCodes;
import com.example.android.mediacenter.Misc.RequestMediaPlayerFunction;
import com.example.android.mediacenter.Misc.RequestQuery;
import com.example.android.mediacenter.R;
import com.example.android.mediacenter.Activities.SongPlayback;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.LinkedList;

/**
 * The fragment to use when loading a songs view in the music library.
 *
 * @author TeamSDK
 * @since November 15 2015
 * @version November 28 2015
 */
public class SongFragment extends Fragment
                          implements SwipeRefreshLayout.OnRefreshListener {

    private static final String DATA_STORAGE = "MyDataFile"; // store data required
    private static final String TAG = "SongFragment";
    private static final String MEDIA_TYPE = "s";

    private static final int NEW_PLAYLIST_COMMAND = 0;
    private static final int SONG_PORT = 4001;

    private SwipeRefreshLayout swipeLayout; // our refreshing swipe layout we are using
    private LinkedList<SongDescriptor> listOfSongs = new LinkedList<>(); // our songs
    private String mediaCenterAddress;
    private QueryCodes query; // the type of query this fragment will issue
    private Boolean needsID; // if a database id field is required in this fragment
    private int id; // the database id if required for potential artist or album
    private SongAdapter songAdapter;

    /**
     * Called to have the fragment instantiate its user interface view.
     *
     * @param inflater - the layoutinflater object that can be used to inflate any views in the fragment
     * @param container - this is the parent view that the fragment's UI should be attached to
     * @param savedInstanceState - the previous state to re-construct this fragment from.
     * @return The view for the fragment's UI
     *
     * @since November 15 2015
     * @version November 28 2015
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_song, container, false); // get the view
        SharedPreferences settings = this.getActivity().getSharedPreferences(DATA_STORAGE, 0);
        Bundle bundle = getArguments();

        ListView listView = (ListView) view.findViewById(R.id.list_music_songs);

        swipeLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_songs_container);
        swipeLayout.setOnRefreshListener(this);

        // Restore currently selected device
        mediaCenterAddress = settings.getString("currentDeviceIP", "");

        // retrieve information needed in this fragment
        query = QueryCodes.values()[bundle.getInt("query")];
        needsID = bundle.getBoolean("needsID");

        if (needsID) {
            id = bundle.getInt("ID");
        }

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            /**
             * The following method handles the clicking of songs in the list of songs.
             *
             * @param parent - The AdapterView where the click happened.
             * @param view - The view within the AdapterView that was clicked.
             * @param position - The position of the view in the adapter.
             * @param id - The row id of the item that was clicked.
             * @return NONE
             *
             * @since November 15 2015
             * @version November 25 2015
             */
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                new SendStartPlaylist().execute(position);

            } // end method

        } // end anonymous class
        );

        songAdapter = new SongAdapter(getActivity().getBaseContext(), R.layout.song_list_item, listOfSongs);
        listView.setAdapter(songAdapter);

        new GetSongsList().execute();

        return view;

    } // end method

    /**
     * This is the handler for pulling down for refresh.
     *
     * @return NONE
     *
     * @since November 15 2015
     * @version November 28 2015
     */
    @Override
    public void onRefresh() {

        listOfSongs.clear();
        songAdapter.notifyDataSetChanged();
        new GetSongsList().execute();

    } // end method

    /**
     * This is contains the task to be run when a pull down for refresh is done.
     *
     * @author TeamSDK
     * @since November 16 2015
     * @version November 28 2015
     */
    private class GetSongsList extends AsyncTask<Void, Void, Void> {

        /**
         * Following defines our background task to be done for the refresh and when entering
         * the fragment.
         *
         * @param params - the parameters of the task
         * @return A result.
         *
         * @since November 16 2015
         * @version November 28 2015
         */
        @SuppressWarnings("unchecked")
        @Override
        protected Void doInBackground(Void... params) {

            RequestQuery songs = new RequestQuery(mediaCenterAddress, SONG_PORT);

            if (needsID) { // query the media center for songs for an artist or album
                songs.sendQueryRequest(query, id);
            }
            else { // query the media center for all songs
                songs.sendQueryRequest(query);
            }

            try {
                ObjectInputStream iStream = songs.receiveRequestedInformation();
                listOfSongs.addAll((LinkedList<SongDescriptor>) iStream.readObject());
                iStream.close();
            } catch (ClassNotFoundException | IOException e) {
                Log.e(TAG, "Ran into following exception when processing byte stream from database:", e);
                e.printStackTrace();
            }

            return null;

        } // end method

        /**
         * Runs on the UI thread after doInBackground finishes its task.
         *
         * @param result - the result of the operations computed by doInBackground
         * @return NONE
         *
         * @since November 16 2015
         * @version November 28 2015
         */
        @Override
        protected void onPostExecute(Void result) {

            songAdapter.notifyDataSetChanged();
            swipeLayout.setRefreshing(false);
            super.onPostExecute(result);

        } // end method

    } // end class

    /**
     * This is contains the task to be run when a song is selected.
     *
     * @author TeamSDK
     * @since November 25 2015
     * @version November 27 2015
     */
    private class SendStartPlaylist extends AsyncTask<Integer, Void, Void> {

        /**
         * Following defines our background task to be done for the refresh and when entering
         * the fragment.
         *
         * @param params - the parameters of the task
         * @return A result.
         *
         * @since November 25 2015
         * @version November 27 2015
         */
        @Override
        protected Void doInBackground(Integer... params) {

            RequestMediaPlayerFunction startSong = new RequestMediaPlayerFunction(mediaCenterAddress);

            String playlist = new String();
            SongDescriptor song = listOfSongs.get(params[0]);
            int songID = song.getId();

            for (SongDescriptor s : listOfSongs) {
                playlist = playlist + s.getId();

                if (s.getId() != listOfSongs.getLast().getId()) {
                    playlist = playlist + "$";
                }

            }

            startSong.sendStartPlaybackRequest(MediaPlayerCodes.PLAYLIST_COMMAND, NEW_PLAYLIST_COMMAND, MEDIA_TYPE, playlist, songID, "");
            startSong.closeConnection();
            return null;

        } // end method

        /**
         * Runs on the UI thread after doInBackground finishes its task.
         *
         * @param result - the result of the operations computed by doInBackground
         * @return NONE
         *
         * @since November 25 2015
         * @version November 27 2015
         */
        @Override
        protected void onPostExecute(Void result) {

            Intent intent = new Intent(getActivity(), SongPlayback.class);
            startActivity(intent);
            super.onPostExecute(result);

        } // end method

    } // end class

} // end class