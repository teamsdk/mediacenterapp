package com.example.android.mediacenter.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.example.android.mediacenter.R;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

/**
 * This is the Pairing menu for the Media Center. Here the user can pair their app with a media
 * center. The user can navigate to different menus depending on their desired usage from here.
 *
 * @author  TeamSDK
 * @since   October 21 2015
 * @version November 28 2015
 */
public class PairingMenu extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final int PAIR_PORT = 3000;

    // UI references.
    private EditText mIPView; // text area to enter in an IP address
    private EditText mAliasView; // text area to enter an alias for this IP address
    private Button mIPButton;

    /**
     * This is the method invoked during the creation of this activity.
     *
     * @param savedInstanceState - The saved state this activity will be able to restore itself to
     *                           under special circumstances.
     * @return NONE
     *
     * @since October 21 2015
     * @version November 6 2015
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState); // try and create from saved state. If not, new state
        setContentView(R.layout.activity_pairing_menu); // load the layout
        Toolbar toolbar = (Toolbar) findViewById(R.id.pairing_toolbar); // get toolbar

        mIPView = (EditText) findViewById(R.id.IP);
        mAliasView = (EditText) findViewById(R.id.Alias);
        mIPButton = (Button) findViewById(R.id.pair_button);
        findViewById(R.id.pbHeaderProgress).setVisibility(View.GONE);
        mIPButton.setOnClickListener(new View.OnClickListener() {

            /**
             * The following method determines the action when the 'Pair' button is clicked.
             *
             * @param view - The View that was clicked.
             * @return NONE
             *
             * @since November 6 2015
             * @version November 6 2015
             */
            @Override
            public void onClick(View view) {

                hideSoftKeyboard(PairingMenu.this);

                if(validateFields()) {
                    mIPView.setEnabled(false);
                    mAliasView.setEnabled(false);
                    mIPButton.setEnabled(false);
                    findViewById(R.id.pbHeaderProgress).setVisibility(View.VISIBLE);
                    new Pair().execute(mIPView.getText().toString(), mAliasView.getText().toString());
                }

            } // end method

        } // end anonymous class
        );

        setSupportActionBar(toolbar); // set toolbar actions
        /*
         * Setup the drawer within this activity and the navigation. Navigation is controlled via
         * the navigation item selected listener.
         */
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.pairing_menu_drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

    } // end method

    /**
     * The following method is used to validate the information the user entered into the IP Address
     * box and the alias box. This method will highlight the invalid field(s) that the user entered.
     *
     * @return The success (true) or failure (false) of the validation.
     *
     * @since November 27 2015
     * @version November 28 2015
     */
    private boolean validateFields() {

        // Reset errors.
        mIPView.setError(null);
        mAliasView.setError(null);

        // Store values at the time of the pair attempt.
        String ipAddr = mIPView.getText().toString();
        String alias = mAliasView.getText().toString();
        boolean cancel = false;
        View focusView = null;

        // Check for a IP address.
        if (TextUtils.isEmpty(ipAddr)) { // nothing entered
            mIPView.setError(getString(R.string.error_field_required));
            focusView = mIPView;
            cancel = true;
        }
        else if (!isIPValid(ipAddr)) { // not a valid IP address
            mIPView.setError(getString(R.string.error_invalid_IP));
            focusView = mIPView;
            cancel = true;
        }

        if (TextUtils.isEmpty(alias)) {
            mAliasView.setError(getString(R.string.error_field_required));
            focusView = mAliasView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the last form field with an error.
            focusView.requestFocus();
            return false;
        }

        return true;

    } // end method

    /**
     * The following method handles what happens in this menu when the back button is pressed.
     *
     * @return NONE
     *
     * @since October 21 2015
     * @version October 30 2015
     */
    @Override
    public void onBackPressed() {

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.pairing_menu_drawer_layout);

        if (drawer.isDrawerOpen(GravityCompat.START)) { // if the navigation view is open, close it
            drawer.closeDrawer(GravityCompat.START);
        } else { // go back to the main menu
            super.onBackPressed();
        }

    } // end method

    /**
     * The following method is the listener for the navigation view and determines the action
     * based on which button is hit in the drawer.
     *
     * @param item - The item that was selected in the navigation view.
     * @return true
     *
     * @since October 21 2015
     * @version October 30 2015
     */
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        int id = item.getItemId(); // retrieve the ID of the navigation view button pressed

        if (id == R.id.nav_home) { // if home button pressed
            Intent intent = new Intent(this, MainActivity.class); // set launch of activity
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent); // start the new activity
            finish(); // end the current activity
        }
        else if (id == R.id.nav_music) { // if pairing button pressed
            Intent intent = new Intent(this, MusicLibrary.class); // set launch of activity
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent); // start the new activity
            finish(); // end the current activity
        }
        else if (id == R.id.nav_video) { // if video library button pressed
            Intent intent = new Intent(this, VideoLibrary.class); // set launch of activity
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent); // start the new activity
            finish(); // end the current activity
        }
        else if (id == R.id.nav_config) { // if music library button pressed
            Intent intent = new Intent(this, ConfigurationMenu.class); // set launch of activity
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent); // start the new activity
            finish(); // end the current activity
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.pairing_menu_drawer_layout);
        drawer.closeDrawer(GravityCompat.START); // close the view after navigation
        return true;

    } // end method

    /**
     * The following method handles what happens when the 'Pair' button is pressed. Will handle
     * situations where the user has not provided enough or incorrect data.
     *
     * @param ipAddr - the IP address of the media center the user is trying to pair with
     * @param alias - the nickname the user is giving to this media center.
     * @return NONE
     *
     * @since November 6 2015
     * @version November 6 2015
     */
    public boolean attemptPairing(String ipAddr, String alias) throws UnknownHostException {

        boolean notPaired = false;

        // set network permissions
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                .permitAll().build();
        StrictMode.setThreadPolicy(policy);

        try {
            DatagramSocket sock = new DatagramSocket(4000);
            byte[] sendBuf = new byte[100];
            byte[] receiveBuf = new byte[100];
            DatagramPacket sendMessage = new DatagramPacket(sendBuf, sendBuf.length, InetAddress.getByName(ipAddr), PAIR_PORT);
            DatagramPacket receiveMessage = new DatagramPacket(receiveBuf, receiveBuf.length);
            sock.setSoTimeout(10000);

                try {
                    sock.send(sendMessage);
                    sock.receive(receiveMessage);
                }
                catch (SocketTimeoutException e) {
                    notPaired = true;
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            sock.close();
        } catch (SocketException e) {
            e.printStackTrace();
        }

        if (notPaired) {
            return false;
        }
        else {
            Intent intent = new Intent(PairingMenu.this, ConfigurationMenu.class);
            intent.putExtra("alias",alias);
            intent.putExtra("ipAddr",ipAddr);
            startActivity(intent);
            finish();
        }
        return true;
    } // end method

    /**
     * The following method determines whether or not the IP address the user entered follows the
     * proper format of an IP address.
     *
     * @param IP - The IP address which the user entered in.
     * @return A boolean indicating if a proper IP address was entered or not.
     *
     * @since November 6 2015
     * @version November 6 2015
     */
    private boolean isIPValid(String IP) {

        return IP.matches("^\\d{1,3}[.]\\d{1,3}[.]\\d{1,3}[.]\\d{1,3}");

    } // end method

    /**
     * The following method will hide the on screen keyboard when called.
     *
     * @param activity - our activity
     * @return NONE
     *
     * @since November 25 2015
     * @version November 28 2015
     */
    private void hideSoftKeyboard(Activity activity) {

        InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Activity.INPUT_METHOD_SERVICE);

        try {
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }


    } // end method

    /**
     * This is contains the task to be run when a pull down for refresh is done.
     *
     * @author TeamSDK
     * @since November 16 2015
     * @version November 19 2015
     */
    private class Pair extends AsyncTask<String, Void, Boolean> {

        /**
         * Following defines our background task to be done for the refresh and when entering
         * the fragment.
         *
         * @param params - the parameters of the task
         * @return A result.
         *
         * @since November 16 2015
         * @version November 19 2015
         */
        @Override
        protected Boolean doInBackground(String... params) {

            try {
                return attemptPairing(params[0], params[1]);
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }

            return false;

        } // end method

        /**
         * Runs on the UI thread after doInBackground finishes its task.
         *
         * @param result - the result of the operations computed by doInBackground
         * @return NONE
         *
         * @since November 16 2015
         * @version November 19 2015
         */
        @Override
        protected void onPostExecute(Boolean result) {

            mIPButton.setEnabled(true);
            mIPView.setEnabled(true);
            mAliasView.setEnabled(true);
            findViewById(R.id.pbHeaderProgress).setVisibility(View.GONE);

            if (!result) {
                Toast toast = Toast.makeText(getApplicationContext(), "Unable to pair, press pairing button on Media Center", Toast.LENGTH_SHORT);
                toast.show();
            }
            else {
                Toast toast = Toast.makeText(getApplicationContext(), "Pair Successful", Toast.LENGTH_SHORT);
                toast.show();
            }

        } // end method

    } // end class

} // end class