package com.example.android.mediacenter.Fragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import com.example.android.mediacenter.Activities.CategoryActivity;
import com.example.android.mediacenter.Adapters.CategoryAdapter;
import shared.CategoryDescriptor;
import com.example.android.mediacenter.Misc.QueryCodes;
import com.example.android.mediacenter.Misc.RequestQuery;
import com.example.android.mediacenter.R;
import java.io.ObjectInputStream;
import java.util.LinkedList;

/**
 * The fragment to use when loading a categories view in the Video Library.
 *
 * @author TeamSDK
 * @since November 15 2015
 * @version November 28 2015
 */
public class CategoryFragment extends Fragment
                              implements SwipeRefreshLayout.OnRefreshListener {

    private static final String DATA_STORAGE = "MyDataFile"; // store data require
    private static final int CATEGORY_PORT = 4005;

    private SwipeRefreshLayout swipeLayout; // our refreshing swipe layout we are using
    private CategoryAdapter categoryAdapter;
    private LinkedList<CategoryDescriptor> listOfCategories = new LinkedList<>(); // our categories
    private String mediaCenterAddress;

    /**
     * Called to have the fragment instantiate its user interface view.
     *
     * @param inflater - the layoutinflater object that can be used to inflate any views in the fragment
     * @param container - this is the parent view that the fragment's UI should be attached to
     * @param savedInstanceState - the previous state to re-construct this fragment from.
     * @return The view for the fragment's UI
     *
     * @since November 15 2015
     * @version November 28 2015
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_category, container, false); // get the view
        SharedPreferences settings = this.getActivity().getSharedPreferences(DATA_STORAGE, 0);

        ListView listView = (ListView) view.findViewById(R.id.list_video_categories);

        swipeLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_categories_container);
        swipeLayout.setOnRefreshListener(this);

        // Restore currently selected device
        mediaCenterAddress = settings.getString("currentDeviceIP", "");

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            /**
             * The following method handles the clicking of categories in the listview of categories.
             *
             * @param parent - The AdapterView where the click happened.
             * @param view - The view within the AdapterView that was clicked.
             * @param position - The position of the view in the adapter.
             * @param id - The row id of the item that was clicked.
             * @return NONE
             *
             * @since November 15 2015
             * @version November 19 2015
             */
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                CategoryDescriptor category = (CategoryDescriptor) parent.getItemAtPosition(position);
                Intent intent = new Intent(getActivity(), CategoryActivity.class); // set launch of activity
                intent.putExtra("categoryName", category.getCategoryName());
                startActivity(intent); // start the new activity

            } // end method

        } // end anonymous class
        );

        categoryAdapter = new CategoryAdapter(getActivity().getBaseContext(), R.layout.category_list_item, listOfCategories);
        listView.setAdapter(categoryAdapter);

        new GetCategoriesList().execute();

        return view;

    } // end method

    /**
     * This is the handler for pulling down for refresh.
     *
     * @return NONE
     *
     * @since November 15 2015
     * @version November 28 2015
     */
    @Override
    public void onRefresh() {

        listOfCategories.clear();
        categoryAdapter.notifyDataSetChanged();
        new GetCategoriesList().execute();

    } // end method

    /**
     * This is contains the task to be run when a pull down for refresh is done and when entering
     * the fragment.
     *
     * @author TeamSDK
     * @since November 16 2015
     * @version November 28 2015
     */
    private class GetCategoriesList extends AsyncTask<Void, Void, Void> {

        /**
         * Following defines our background task to be done.
         *
         * @param params - the parameters of the task
         * @return A result.
         *
         * @since November 16 2015
         * @version November 28 2015
         */
        @SuppressWarnings("unchecked")
        @Override
        protected Void doInBackground(Void... params) {

            RequestQuery categories = new RequestQuery(mediaCenterAddress, CATEGORY_PORT);
            categories.sendQueryRequest(QueryCodes.CATEGORIES_QUERY);

            try {
                ObjectInputStream iStream = categories.receiveRequestedInformation();
                listOfCategories.addAll((LinkedList<CategoryDescriptor>) iStream.readObject());
                iStream.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;

        } // end method

        /**
         * Runs on the UI thread after doInBackground finishes its task.
         *
         * @param result - the result of the operations computed by doInBackground
         * @return NONE
         *
         * @since November 16 2015
         * @version November 28 2015
         */
        @Override
        protected void onPostExecute(Void result) {

            categoryAdapter.notifyDataSetChanged();
            swipeLayout.setRefreshing(false);
            super.onPostExecute(result);

        } // end method

    } // end class

} // end class