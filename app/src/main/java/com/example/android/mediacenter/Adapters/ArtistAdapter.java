package com.example.android.mediacenter.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import shared.ArtistDescriptor;
import com.example.android.mediacenter.R;

import java.util.List;

/**
 *  The following class is the adapter used in the Music Library for artist view. The adapter
 *  controls the setup of the list of artists being displayed in the listview.
 *
 * @author  TeamSDK
 * @since   November 15 2015
 * @version November 15 2015
 */
public class ArtistAdapter extends ArrayAdapter<ArtistDescriptor> {

    /**
     * The constructor for CategoryAdapter used to setup the ListView.
     *
     * @param context - The current context.
     * @param resource - The resource ID for a layout file containing a list_view ot use when
     *                 instantiating views.
     * @param artists - The objects to represent in the ListView.
     *
     * @since   November 15 2015
     * @version November 15 2015
     */
    public ArtistAdapter(Context context, int resource, List<ArtistDescriptor> artists) {

        super(context, resource, artists); // use the constructor for ArrayAdapter.

    } // end constructor

    /**
     * The following method is used to get a View that display the data at the specified position in
     * the data set.
     *
     * @param position - The position of the item within the adapter's data set of the item whose
     *                 view we want.
     * @param convertView - The old view to reuse, if possible.
     * @param parent - The parent that this view will eventually be attached to.
     * @return A View corresponding to the data at the specified position.
     *
     * @since   November 15 2015
     * @version November 15 2015
     */
    @SuppressLint({"SetTextI18n", "InflateParams"})
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView; // use the old view

        if (v == null) { // if the old view is not available
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.artist_list_item, null); // inflate from a XML layout file.
        }

        ArtistDescriptor artist = getItem(position); // get the ArtistDescriptor at the current position.

        if (artist != null) { // if our ArtistDescriptor is valid
            // retrieve the TextView placeholders within artist_list_item to display artist parameters
            TextView tt1 = (TextView) v.findViewById(R.id.artist_name);

            if (tt1 != null) { // if our TextView is valid
                tt1.setText(artist.getArtistName()); // set the name of the artist
            }
        }

        return v;

    } // end method

} // end class