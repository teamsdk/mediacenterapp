package com.example.android.mediacenter.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.android.mediacenter.R;
import shared.VideoDescriptor;

import java.util.List;

/**
 *  The following class is the adapter used in the video library listview for videos. The adapter
 *  controls the setup of the list of videos being displayed in the listview.
 *
 * @author  TeamSDK
 * @since   November 3 2015
 * @version November 3 2015
 */
public class VideoAdapter extends ArrayAdapter<VideoDescriptor> {

    /**
     * The constructor for VideoAdapter used to setup the ListView.
     *
     * @param context - The current context.
     * @param resource - The resource ID for a layout file containing a list_view ot use when
     *                 instantiating views.
     * @param videos - The objects to represent in the ListView.
     *
     * @since   November 3 2015
     * @version November 3 2015
     */
    public VideoAdapter(Context context, int resource, List<VideoDescriptor> videos) {

        super(context, resource, videos); // use the constructor for ArrayAdapter.

    } // end constructor

    /**
     * The following method is used to get a View that display the data at the specified position in
     * the data set.
     *
     * @param position - The position of the item within the adapter's data set of the item whose
     *                 view we want.
     * @param convertView - The old view to reuse, if possible.
     * @param parent - The parent that this view will eventually be attached to.
     * @return A View corresponding to the data at the specified position.
     *
     * @since   November 3 2015
     * @version November 3 2015
     */
    @SuppressLint({"SetTextI18n", "InflateParams"})
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView; // use the old view

        if (v == null) { // if the old view is not available
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.video_list_item, null); // inflate from a XML layout file.
        }

        VideoDescriptor video = getItem(position); // get the VideoDescriptor at the current position.

        if (video != null) { // if our VideoDescriptor is valid
            // retrieve the TextView placeholders within video_list_item to display video parameters
            TextView tt1 = (TextView) v.findViewById(R.id.video_title);
            TextView tt2 = (TextView) v.findViewById(R.id.video_category_year);

            if (tt1 != null) { // if our TextView is valid
                tt1.setText(video.getTitle()); // set the title of the video
            }

            if (tt2 != null) { // if our TextView is valid
                tt2.setText(video.getCategory() + " - " + video.getReleaseYear()); // set the category & release year
            }
        }

        return v;

    } // end method

} // end class