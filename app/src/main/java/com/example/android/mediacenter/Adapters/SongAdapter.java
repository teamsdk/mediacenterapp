package com.example.android.mediacenter.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.android.mediacenter.R;
import shared.SongDescriptor;

import java.util.List;

/**
 *  The following class is the adapter used in the music library listview for songs. The adapter
 *  controls the setup of the list of songs being displayed in the listview.
 *
 * @author  TeamSDK
 * @since   October 25 2015
 * @version November 3 2015
 */
public class SongAdapter extends ArrayAdapter<SongDescriptor> {

    /**
     * The constructor for SongAdapter used to setup the ListView.
     *
     * @param context - The current context.
     * @param resource - The resource ID for a layout file containing a list_view ot use when
     *                 instantiating views.
     * @param songs - The objects to represent in the ListView.
     *
     * @since   October 25 2015
     * @version October 30 2015
     */
    public SongAdapter(Context context, int resource, List<SongDescriptor> songs) {

        super(context, resource, songs); // use the constructor for ArrayAdapter.

    } // end constructor

    /**
     * The following method is used to get a View that display the data at the specified position in
     * the data set.
     *
     * @param position - The position of the item within the adapter's data set of the item whose
     *                 view we want.
     * @param convertView - The old view to reuse, if possible.
     * @param parent - The parent that this view will eventually be attached to.
     * @return A View corresponding to the data at the specified position.
     *
     * @since   October 25 2015
     * @version November 3 2015
     */
    @SuppressLint({"SetTextI18n", "InflateParams"})
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView; // use the old view

        if (v == null) { // if the old view is not available
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.song_list_item, null); // inflate from a XML layout file.
        }

        SongDescriptor song = getItem(position); // get the SongDescriptor at the current position.
        if (song != null) { // if our SongDescriptor is valid
            // retrieve the TextView placeholders within song_list_item to display song parameters
            TextView tt1 = (TextView) v.findViewById(R.id.song_title);
            TextView tt2 = (TextView) v.findViewById(R.id.song_artist_album);
            TextView tt3 = (TextView) v.findViewById(R.id.song_year_track);

            if (tt1 != null) { // if our TextView is valid
                tt1.setText(song.getTitle()); // set the title of the song
            }

            if (tt2 != null) { // if our TextView is valid
                tt2.setText(song.getArtist() + " - " + song.getAlbum()); // set the artist & album
            }

            if (tt3 != null) { // if our TextView is valid
                tt3.setText(song.getReleaseYear() + " - " + song.getTrackNumber()); // set the release year & track number
            }
        }

        return v;

    } // end method

} // end class