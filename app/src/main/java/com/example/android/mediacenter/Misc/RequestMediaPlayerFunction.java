package com.example.android.mediacenter.Misc;

import android.os.StrictMode;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

/**
 * The class to request playback functions and receive information from the media center we are
 * connected to.
 *
 * @author TeamSDK
 * @since November 25 2015
 * @version December 6 2015
 */
public class RequestMediaPlayerFunction {

    private final static int COMMAND_PORT = 5000;
    private final static int DATA_MAX_LENGTH = 1024;
    private final static String PLAY_NEW_PLAYLIST_COMMAND = "[%d][%d][%s][%s][%d][%s]";
    private final static String MEDIA_PLAYER_COMMAND = "[%d][%d]";
    private final static String GET_CURRENT_COMMAND = "[%d][%s]";

    private InetAddress mediaCenterAddress; // the media center we are communicating with
    private DatagramSocket sendReceiveSock;

    /**
     * Constructor to setup our RequestQuery object with our media centers IP address.
     *
     * @param ipAddr - the IP address of the media center we are communicating with
     *
     * @since November 25 2015
     * @version November 25 2015
     */
    public RequestMediaPlayerFunction(String ipAddr) {

        // set network permissions
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                .permitAll().build();
        StrictMode.setThreadPolicy(policy);

        try {
            mediaCenterAddress = InetAddress.getByName(ipAddr);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        try {
            sendReceiveSock = new DatagramSocket();
        } catch (SocketException e) {
            e.printStackTrace();
        }

    } // end constructor

    /**
     * Used to send a query with no extra parameters to the media center.
     *
     * @param commandType - our command op code
     * @param command - the type of sub command
     * @return NONE
     *
     * @since November 25 2015
     * @version November 25 2015
     */
    public void sendStartPlaybackRequest(MediaPlayerCodes commandType, int command, String mediaType, String playlist, int current, String options) {

        String message = String.format(PLAY_NEW_PLAYLIST_COMMAND, commandType.ordinal(), command, mediaType, playlist, current, options);
        byte[] packetData = message.getBytes();

        DatagramPacket sendPack = new DatagramPacket(packetData, packetData.length, mediaCenterAddress, COMMAND_PORT);

        try {
            sendReceiveSock.send(sendPack);
        } catch (IOException e) {
            e.printStackTrace();
        }

    } // end method

    /**
     * Used to send a query with no extra parameters to the media center.
     *
     * @param commandType - our command op code
     * @param command - the type of sub command
     * @return NONE
     *
     * @since November 25 2015
     * @version November 25 2015
     */
    public void sendCommandRequest(MediaPlayerCodes commandType, int command) {

        String message = String.format(MEDIA_PLAYER_COMMAND, commandType.ordinal(), command);
        byte[] packetData = message.getBytes();

        DatagramPacket sendPack = new DatagramPacket(packetData, packetData.length, mediaCenterAddress, COMMAND_PORT);

        try {
            sendReceiveSock.send(sendPack);
        } catch (IOException e) {
            e.printStackTrace();
        }

    } // end method

    /**
     * Used to send a query with no extra parameters to the media center.
     *
     * @param commandType - our command op code
     * @param mediaType - the type of sub command
     * @return NONE
     *
     * @since November 28 2015
     * @version November 28 2015
     */
    public void sendCommandRequest(MediaPlayerCodes commandType, String mediaType) {

        String message = String.format(GET_CURRENT_COMMAND, commandType.ordinal(), mediaType);
        byte[] packetData = message.getBytes();

        DatagramPacket sendPack = new DatagramPacket(packetData, packetData.length, mediaCenterAddress, COMMAND_PORT);

        try {
            sendReceiveSock.send(sendPack);
        } catch (IOException e) {
            e.printStackTrace();
        }

    } // end method

    /**
     * Retrieves a byte array of information corresponding to what we requested in our
     * sendQueryRequest.
     *
     * @return The byte array corresponding our information we requested from the media center.
     *
     * @since November 25 2015
     * @version November 25 2015
     */
    public byte[] receiveRequestedInformation() {

        byte[] receiveData = new byte[DATA_MAX_LENGTH];
        DatagramPacket recvPack = new DatagramPacket(receiveData, receiveData.length);
        int communicationPort;
        byte[] data;

        try {
            sendReceiveSock.receive(recvPack);
        } catch (IOException e) {
            e.printStackTrace();
        }
        communicationPort = recvPack.getPort();
        data = recvPack.getData();
        DatagramPacket ackPack = new DatagramPacket("ack".getBytes(), 3, mediaCenterAddress, communicationPort);

        try {
            sendReceiveSock.send(ackPack);
        } catch (IOException e) {
            e.printStackTrace();
        }

        sendReceiveSock.close();
        return data;

    } // end method

    /**
     * Closes the socket since we are now done with it
     *
     * @return NONE
     *
     * @since December 6 2015
     * @version December 6 2015
     */
    public void closeConnection() {

        sendReceiveSock.close();

    } // end method

} // end class