package com.example.android.mediacenter.Activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import com.example.android.mediacenter.Adapters.MCDAdapter;
import com.example.android.mediacenter.Descriptors.MediaCenterDescriptor;
import com.example.android.mediacenter.R;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.UnknownHostException;
import java.util.LinkedList;

/**
 * This is the Configuration menu for the Media Center. It gives the user options to configure with
 * the app and the player. The user can navigate to different menus depending on their desired usage
 * from here.
 *
 * @author  TeamSDK
 * @since   October 21 2015
 * @version November 28 2015
 */
public class ConfigurationMenu extends AppCompatActivity
                               implements NavigationView.OnNavigationItemSelectedListener {

    private static final String MEDIA_CENTERS = "MediaCenters"; // store configured media centers
    private static final String DATA_STORAGE = "MyDataFile"; // store data required

    private LinkedList<MediaCenterDescriptor> listOfDevices = new LinkedList<>();
    private MCDAdapter mcdAdapter; // Adapter for media center descriptor
    private String currentDevice; // the current device we are controlling
    private String currentDeviceIP; // the IP address for the device we are controlling

    /**
     * This is the method invoked during the creation of this activity.
     *
     * @param savedInstanceState - The saved state this activity will be able to restore itself to
     *                           under special circumstances.
     * @return NONE
     *
     * @since October 21 2015
     * @version November 13 2015
     */
    @SuppressWarnings("unchecked")
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState); // try and create from saved state. If not, new state
        setContentView(R.layout.activity_configuration_menu); // load the layout

        String alias;
        String ipAddr;
        Toolbar toolbar = (Toolbar) findViewById(R.id.configuration_toolbar); // get toolbar
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.config_menu_drawer_layout);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        ListView listView = (ListView) findViewById(R.id.list_media_centers);
        mcdAdapter = new MCDAdapter(this, R.layout.media_center_list_item, listOfDevices);
        Bundle extras = getIntent().getExtras(); // retrieve potential media center information

        // Restore currently selected device
        SharedPreferences settings = getSharedPreferences(DATA_STORAGE, 0);
        currentDevice = settings.getString("currentDevice", "");
        currentDeviceIP = settings.getString("currentDeviceIP", "");
        TextView current = (TextView) findViewById(R.id.media_center_current);
        current.setText(currentDevice);

        FileInputStream fis;
        try {
            fis = openFileInput(MEDIA_CENTERS);
            ObjectInputStream ois = new ObjectInputStream(fis);
            listOfDevices.addAll((LinkedList<MediaCenterDescriptor>) ois.readObject());
            ois.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (extras != null) { // if we received information from previous activity
            alias = extras.getString("alias"); // get the song title
            ipAddr = extras.getString("ipAddr"); // get the song artist & album

            try {
                listOfDevices.add(new MediaCenterDescriptor(alias, ipAddr));
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }
        }

        setSupportActionBar(toolbar); // set toolbar actions
        /*
         * Setup the drawer within this activity and the navigation. Navigation is controlled via
         * the navigation item selected listener.
         */
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            /**
             * The following method handles the clicking of songs in the list of songs.
             *
             * @param parent - The AdapterView where the click happened.
             * @param view - The view within the AdapterView that was clicked.
             * @param position - The position of the view in the adapter.
             * @param id - The row id of the item that was clicked.
             * @return NONE
             *
             * @since October 26 2015
             * @version October 30 2015
             */
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                MediaCenterDescriptor device = (MediaCenterDescriptor) parent.getItemAtPosition(position);
                TextView current = (TextView) findViewById(R.id.media_center_current);
                currentDevice = device.getAlias();
                currentDeviceIP = device.getIpAddr();
                current.setText(currentDevice);

            } // end method

        } // end anonymous class
        );

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            /**
             * The following method handles the long clicking of a media center
             *
             * @param parent - The AdapterView where the click happened.
             * @param view - The view within the AdapterView that was clicked.
             * @param position - The position of the view in the adapter.
             * @param id - The row id of the item that was clicked.
             * @return NONE
             *
             * @since November 8 2015
             * @version November 13 2015
             */
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                AlertDialog alertDialog = new AlertDialog.Builder(view.getContext()).create();
                alertDialog.setTitle("Options");
                final int positionToRemove = position;
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Remove", new DialogInterface.OnClickListener() {

                    /**
                     * The click listener for the remove button.
                     *
                     * @param dialog - The dialog that received the click.
                     * @param which - The button which was pressed.
                     * @return NONE
                     *
                     * @since November 8 2015
                     * @version November 13 2015
                     */
                    public void onClick(DialogInterface dialog, int which) {

                        listOfDevices.remove(positionToRemove);
                        mcdAdapter.notifyDataSetChanged();

                    } // end method

                } // end anonymous class
                );

                alertDialog.show();
                return true;

            } // end method

        } // end anonymous class
        );

        listView.setAdapter(mcdAdapter);

    } // end method

    /**
     * The following method handles what happens in this activity when it is stopped.
     *
     * @return NONE
     *
     * @since November 8 2015
     * @version November 13 2015
     */
    @Override
    public void onStop() {

        super.onStop();

        // store all the configured media centers
        FileOutputStream fos = null;

        try {
            fos = openFileOutput(MEDIA_CENTERS, Context.MODE_PRIVATE);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        ObjectOutputStream oos;

        try {
            oos = new ObjectOutputStream(fos);
            oos.writeObject(listOfDevices);
            oos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    } // end method

    /**
     * The following method handles what happens in this menu when the back button is pressed.
     *
     * @return NONE
     *
     * @since October 21 2015
     * @version October 30 2015
     */
    @Override
    public void onBackPressed() {

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.config_menu_drawer_layout);

        if (drawer.isDrawerOpen(GravityCompat.START)) { // if the navigation view is open, close it
            drawer.closeDrawer(GravityCompat.START);
        }
        else { // go back to the main menu
            super.onBackPressed();
        }

    } // end method

    /**
     * The following method is the listener for the navigation view and determines the action
     * based on which button is hit in the drawer.
     *
     * @param item - The item that was selected in the navigation view.
     * @return true
     *
     * @since October 21 2015
     * @version November 28 2015
     */
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        int id = item.getItemId(); // retrieve the ID of the navigation view button pressed
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.config_menu_drawer_layout);

        // We need an Editor object to make preference changes.
        // All objects are from android.context.Context
        SharedPreferences settings = getSharedPreferences(DATA_STORAGE, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("currentDevice", currentDevice);
        editor.putString("currentDeviceIP", currentDeviceIP);
        // Commit the edits!
        editor.apply();

        if (id == R.id.nav_home) { // if home button pressed
            Intent intent = new Intent(this, MainActivity.class); // set launch of activity
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent); // start the new activity
            finish(); // end the current activity
        }
        else if (id == R.id.nav_music) { // if music library button pressed
            Intent intent = new Intent(this, MusicLibrary.class); // set launch of activity
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent); // start the new activity
            finish(); // end the current activity
        }
        else if (id == R.id.nav_video) { // if video library button pressed
            Intent intent = new Intent(this, VideoLibrary.class); // set launch of activity
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent); // start the new activity
            finish(); // end the current activity
        }
        else if (id == R.id.nav_pair) { // if pairing button pressed
            Intent intent = new Intent(this, PairingMenu.class); // set launch of activity
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent); // start the new activity
            finish(); // end the current activity
        }

        drawer.closeDrawer(GravityCompat.START); // close the view after navigation
        return true;

    } // end method

} // end class