package com.example.android.mediacenter.Descriptors;

import java.io.Serializable;
import java.net.UnknownHostException;
import java.util.Date;

/**
 * The following class is used to store the information pertained to a media center which has been
 * paired with the device.
 *
 * @author  TeamSDK
 * @since   November 6 2015
 * @version November 16 2015
 */
public class MediaCenterDescriptor implements Serializable {

    private static final long serialVersionUID = 465489; // serialization ID

    private String alias; // String to store the alias given to this media center
    private String ipAddr; // the IP address of the media center
    private Date pairDate; // the date that the media center was paired

    /**
     * The main constructor for a media center descriptor object.
     *
     * @param alias - The alias the user entered for the media center
     * @param ipAddr - The IP address the user entered for the media center
     * @throws UnknownHostException - invalid IP address
     *
     * @since November 6 2015
     * @version November 16 2015
     */
    public MediaCenterDescriptor(String alias, String ipAddr) throws UnknownHostException {

        this.alias = alias;
        this.ipAddr = ipAddr;
        pairDate = new Date();

    } // end constructor

    /**
     * Getter method to get alias of the media center
     *
     * @return The alias of the media center
     *
     * @since November 6 2015
     * @version November 6 2015
     */
    public String getAlias() {

        return alias;

    } // end method

    /**
     * Getter method to get IP address of the media center
     *
     * @return The IP address of the media center
     *
     * @since November 6 2015
     * @version November 16 2015
     */
    public String getIpAddr() {

        return ipAddr;

    } // end method

    /**
     * Getter method to get date of the media center
     *
     * @return The date of the media center
     *
     * @since November 6 2015
     * @version November 6 2015
     */
    public Date getPairDate() {

        return pairDate;

    } // end method

    /**
     * Setter method to set alias of media center
     *
     * @param alias - the alias to set for the media center
     * @return NONE
     *
     * @since November 6 2015
     * @version November 6 2015
     */
    public void setAlias(String alias) {

        this.alias = alias;

    } // end method

    /**
     * Setter method to set IP address of media center
     *
     * @param ipAddr - the IP address to set for the media center
     * @return NONE
     *
     * @since November 6 2015
     * @version November 16 2015
     */
    public void setIpAddr(String ipAddr) {

        this.ipAddr = ipAddr;

    } // end method

    /**
     * Setter method to set date paired of media center
     *
     * @param pairDate - the date paired to set for the media center
     * @return NONE
     *
     * @since November 6 2015
     * @version November 6 2015
     */
    public void setPairDate(Date pairDate) {

        this.pairDate = pairDate;

    } // end method

} // end class