package com.example.android.mediacenter.Misc;

/**
 * Enum used to define possible Query Codes for our database.
 *
 * @author TeamSDK
 * @since November 16 2015
 * @version November 16 2015
 */
public enum QueryCodes {

    SONGS_FOR_ALBUM_QUERY,
    SONGS_FOR_ARTIST_QUERY,
    SONGS_QUERY,
    ALBUMS_FOR_ARTIST_QUERY,
    ALBUMS_QUERY,
    ARTISTS_QUERY,
    VIDEOS_QUERY,
    VIDEOS_FOR_CATEGORY_QUERY,
    CATEGORIES_QUERY

} // end enum
