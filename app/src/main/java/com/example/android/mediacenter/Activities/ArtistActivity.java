package com.example.android.mediacenter.Activities;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import com.example.android.mediacenter.Fragments.AlbumFragment;
import com.example.android.mediacenter.Fragments.SongFragment;
import com.example.android.mediacenter.Misc.QueryCodes;
import com.example.android.mediacenter.Misc.ViewPagerAdapter;
import com.example.android.mediacenter.R;

/**
 * Our activity to be used when the user selects an artist from the ARTISTS tab in Music Library.
 *
 * @author TeamSDK
 * @since November 19 2015
 * @version November 28 2015
 */
public class ArtistActivity extends AppCompatActivity {

    private int artistID; // to hold the database id of the artist upon receiving it from previous activity

    /**
     * This is the method invoked during the creation of this activity.
     *
     * @param savedInstanceState - The saved state this activity will be able to restore itself to
     *                           under special circumstances.
     * @return NONE
     *
     * @since November 19 2015
     * @version November 28 2015
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_artist);

        Bundle extras = getIntent().getExtras();
        Toolbar toolbar = (Toolbar) findViewById(R.id.artist_toolbar); // get toolbar
        ViewPager viewPager = (ViewPager) findViewById(R.id.artist_viewpager);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.artist_tabs);
        Button buttonNowPlaying = (Button) findViewById(R.id.now_playing_button_music);

        artistID = extras.getInt("artistID"); // retrieve the database id of the artist
        setTitle(extras.getString("artistName")); // here we set the title of the activity

        // set toolbar functionality here
        setSupportActionBar(toolbar);
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);

        buttonNowPlaying.setOnClickListener(new View.OnClickListener() {

            /**
             * The following method determines the action when the 'Now Playing' button is clicked.
             *
             * @param view - the view that was clicked
             * @return NONE
             *
             * @since November 28 2015
             * @version November 28 2015
             */
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(ArtistActivity.this, SongPlayback.class);
                startActivity(intent);

            } // end method

        } // end anonymous class
        );

    } // end method

    /**
     * Sets up items on the ViewPager for user's view.
     *
     * @param viewPager - the ViewPager to display on
     * @return NONE
     *
     * @since November 19 2015
     * @version November 28 2015
     */
    private void setupViewPager(ViewPager viewPager) {

        Bundle songFragBundle = new Bundle(); // bundle used for our SongFragment
        Bundle albumFragBundle = new Bundle();  // bundle used for our AlbumFragment
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        Fragment songFragment = new SongFragment();
        Fragment albumFragment = new AlbumFragment();

        // information to pass to our SongFragment
        songFragBundle.putInt("query", QueryCodes.SONGS_FOR_ARTIST_QUERY.ordinal());
        songFragBundle.putBoolean("needsID", true);
        songFragBundle.putInt("ID", artistID);

        // information to pass to our AlbumFragment
        albumFragBundle.putInt("query", QueryCodes.ALBUMS_FOR_ARTIST_QUERY.ordinal());
        albumFragBundle.putBoolean("needsID", true);
        albumFragBundle.putInt("artistID", artistID);

        // give Fragments the information they need
        songFragment.setArguments(songFragBundle);
        albumFragment.setArguments(albumFragBundle);

        adapter.addFragment(songFragment, "SONGS");
        adapter.addFragment(albumFragment, "ALBUMS");
        viewPager.setAdapter(adapter);

    } // end method

} // end class