package com.example.android.mediacenter.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import com.example.android.mediacenter.Fragments.AlbumFragment;
import com.example.android.mediacenter.Fragments.ArtistFragment;
import com.example.android.mediacenter.Fragments.SongFragment;
import com.example.android.mediacenter.Misc.QueryCodes;
import com.example.android.mediacenter.Misc.ViewPagerAdapter;
import com.example.android.mediacenter.R;

/**
 * This is the Music Library menu for the Media Center. Here the user can select which song they
 * would like to play, view the current playlist, and view by song view, artist view, or album view.
 * The user can navigate to different menus depending on their desired usage from here.
 *
 * @author  TeamSDK
 * @since   October 21 2015
 * @version November 19 2015
 */
public class MusicLibrary extends AppCompatActivity
                          implements NavigationView.OnNavigationItemSelectedListener {

    /**
     * This is the method invoked during the creation of this activity.
     *
     * @param savedInstanceState - The saved state this activity will be able to restore itself to
     *                           under special circumstances.
     * @return NONE
     *
     * @since October 21 2015
     * @version November 15 2015
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState); // try and create from saved state. If not, new state
        setContentView(R.layout.activity_music_library); // load the layout

        Toolbar toolbar = (Toolbar) findViewById(R.id.music_toolbar); // get toolbar
        ViewPager viewPager = (ViewPager) findViewById(R.id.music_viewpager);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.music_tabs);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.music_library_drawer_layout);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        Button buttonNowPlaying = (Button) findViewById(R.id.now_playing_button_music);

        // set toolbar functionality here
        setSupportActionBar(toolbar);
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);

        buttonNowPlaying.setOnClickListener(new View.OnClickListener() {

            /**
             * The following method determines the action when the 'Now Playing' button is clicked.
             *
             * @param view - the view that was clicked
             * @return NONE
             *
             * @since November 28 2015
             * @version November 28 2015
             */
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(MusicLibrary.this, SongPlayback.class);
                startActivity(intent);

            } // end method

        } // end anonymous class
        );

        /*
         * Setup the drawer within this activity and the navigation. Navigation is controlled via
         * the navigation item selected listener.
         */
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

    } // end method

    /**
     * The following method handles what happens in this menu when the back button is pressed.
     *
     * @return NONE
     *
     * @since October 21 2015
     * @version October 30 2015
     */
    @Override
    public void onBackPressed() {

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.music_library_drawer_layout);

        if (drawer.isDrawerOpen(GravityCompat.START)) { // if the navigation view is open, close it
            drawer.closeDrawer(GravityCompat.START);
        }
        else { // go back to the main menu
            super.onBackPressed();
        }

    } // end method

    /**
     * The following method is the listener for the navigation view and determines the action
     * based on which button is hit in the drawer.
     *
     * @param item - The item that was selected in the navigation view.
     * @return true
     *
     * @since October 21 2015
     * @version October 30 2015
     */
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        int id = item.getItemId(); // retrieve the ID of the navigation view button pressed
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.music_library_drawer_layout);

        if (id == R.id.nav_home) { // if home button pressed
            Intent intent = new Intent(this, MainActivity.class); // set launch of activity
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent); // start the new activity
            finish(); // end the current activity
        }
        else if (id == R.id.nav_video) { // if video library button pressed
            Intent intent = new Intent(this, VideoLibrary.class); // set launch of activity
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent); // start the new activity
            finish(); // end the current activity
        }
        else if (id == R.id.nav_config) { // if music library button pressed
            Intent intent = new Intent(this, ConfigurationMenu.class); // set launch of activity
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent); // start the new activity
            finish(); // end the current activity
        }
        else if (id == R.id.nav_pair) { // if pairing button pressed
            Intent intent = new Intent(this, PairingMenu.class); // set launch of activity
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent); // start the new activity
            finish(); // end the current activity
        }

        drawer.closeDrawer(GravityCompat.START); // close the view after navigation
        return true;

    } // end method

    /**
     * Sets up items on the ViewPager for user's view.
     *
     * @param viewPager - the ViewPager to display on
     * @return NONE
     *
     * @since November 15 2015
     * @version November 19 2015
     */
    private void setupViewPager(ViewPager viewPager) {

        Bundle songFragBundle = new Bundle(); // bundle used for our SongFragment
        Bundle albumFragBundle = new Bundle(); // bundle used for our AlbumFragment
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        Fragment songFragment = new SongFragment();
        Fragment artistFragment = new ArtistFragment();
        Fragment albumFragment = new AlbumFragment();

        // information to pass to our SongFragment
        songFragBundle.putInt("query", QueryCodes.SONGS_QUERY.ordinal());
        songFragBundle.putBoolean("needsID", false);

        // information to pass to our AlbumFragment
        albumFragBundle.putInt("query", QueryCodes.ALBUMS_QUERY.ordinal());
        albumFragBundle.putBoolean("needsID", false);

        // give Fragments the information they need
        songFragment.setArguments(songFragBundle);
        albumFragment.setArguments(albumFragBundle);

        adapter.addFragment(songFragment, "SONGS");
        adapter.addFragment(artistFragment, "ARTISTS");
        adapter.addFragment(albumFragment, "ALBUMS");
        viewPager.setAdapter(adapter);

    } // end method

} // end class