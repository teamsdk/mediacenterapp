package com.example.android.mediacenter.Misc;

import android.os.StrictMode;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.LinkedList;

/**
 * The class to request and receive information from the media center we are connected to. Our
 * fragments will get the information and cast it to the appropriate list of descriptors.
 *
 * @author TeamSDK
 * @since November 16 2015
 * @version November 28 2015
 */
public class RequestQuery {

    private final static int QUERY_PORT = 8008;

    private InetAddress mediaCenterAddress; // the media center we are communicating with
    private DatagramSocket sendReceiveSock;
    private int ourPort;
    private ServerSocket serverSocket;
    private InetSocketAddress inboundAddr;

    /**
     * Constructor to setup our RequestQuery object with our media centers IP address.
     *
     * @param ipAddr - the IP address of the media center we are communicating with
     *
     * @since November 16 2015
     * @version November 28 2015
     */
    public RequestQuery(String ipAddr, int port) {

        // set network permissions
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                .permitAll().build();
        StrictMode.setThreadPolicy(policy);

        try {
            mediaCenterAddress = InetAddress.getByName(ipAddr);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        try {
            sendReceiveSock = new DatagramSocket(port);
        } catch (SocketException e) {
            e.printStackTrace();
        }
        ourPort = port;

    } // end constructor

    /**
     * Used to send a query with no extra parameters to the media center.
     *
     * @param query - our query op code
     * @return NONE
     *
     * @since November 16 2015
     * @version November 16 2015
     */
    public void sendQueryRequest(QueryCodes query) {

        String message = "[" + Integer.toString(query.ordinal()) + "][]";
        byte[] packetData = message.getBytes();

        DatagramPacket sendPack = new DatagramPacket(packetData, packetData.length, mediaCenterAddress, QUERY_PORT);

        try {
            sendReceiveSock.send(sendPack);
        } catch (IOException e) {
            e.printStackTrace();
        }

        sendReceiveSock.close();

    } // end method

    /**
     * Used to send a query with an id parameter to the media center.
     *
     * @param query - our query op code
     * @param id - our extra paramter id to query for when hitting an artist, album, or category
     * @return NONE
     *
     * @since November 16 2015
     * @version November 16 2015
     */
    public void sendQueryRequest(QueryCodes query, int id) {

        String message = "[" + Integer.toString(query.ordinal()) + "][" + Integer.toString(id) + "]";
        byte[] packetData = message.getBytes();

        DatagramPacket sendPack = new DatagramPacket(packetData, packetData.length, mediaCenterAddress, QUERY_PORT);

        try {
            sendReceiveSock.send(sendPack);
        } catch (IOException e) {
            e.printStackTrace();
        }

        sendReceiveSock.close();

    } // end method

    /**
     * Used to send a query with an extra parameter to the media center.
     *
     * @param query - our query op code
     * @param category - our category to query for when hitting a category
     * @return NONE
     *
     * @since November 16 2015
     * @version November 16 2015
     */
    public void sendQueryRequest(QueryCodes query, String category) {

        String message = "[" + Integer.toString(query.ordinal()) + "][" + category + "]";
        byte[] packetData = message.getBytes();

        DatagramPacket sendPack = new DatagramPacket(packetData, packetData.length, mediaCenterAddress, QUERY_PORT);

        try {
            sendReceiveSock.send(sendPack);
        } catch (IOException e) {
            e.printStackTrace();
        }

        sendReceiveSock.close();

    } // end method

    /**
     * Retrieves a byte array of information corresponding to what we requested in our
     * sendQueryRequest.
     *
     * @return The byte array corresponding our information we requested from the media center.
     *
     * @since November 16 2015
     * @version November 28 2015
     */
    public ObjectInputStream receiveRequestedInformation() {

        ObjectInputStream oiStream = null;
        initServerSocket();

        try {
            Socket informationStream = this.serverSocket.accept();
            InputStream iStream = informationStream.getInputStream();
            oiStream = new ObjectInputStream(iStream);
            this.serverSocket.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return oiStream;

    } // end method

    /**
     * Following method is used to initialize our server socket for receiving information from the
     * media center.
     *
     * @return NONE
     *
     * @since November 28 2015
     * @version November 28 2015
     */
    private void initServerSocket() {

        this.inboundAddr = new InetSocketAddress(ourPort);

        try {
            this.serverSocket = new java.net.ServerSocket(ourPort);

            if (this.serverSocket.isBound()) {
                System.out.println("SERVER inbound data port " +
                        this.serverSocket.getLocalPort() +
                        " is ready and waiting for client to connect...");
            }

        } catch (SocketException se) {
            System.err.println("Unable to create socket.");
            System.err.println(se.toString());
            System.exit(1);
        } catch (IOException ioe) {
            System.err.println("Unable to read data from an open socket.");
            System.err.println(ioe.toString());
            System.exit(1);
        }

    } // end method

} // end class