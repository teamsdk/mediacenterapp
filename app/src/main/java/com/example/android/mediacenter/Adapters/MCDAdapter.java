package com.example.android.mediacenter.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.android.mediacenter.Descriptors.MediaCenterDescriptor;
import com.example.android.mediacenter.R;

import java.util.List;

/**
 *  The following class is the adapter used in the configuration menu for media centers. The adapter
 *  controls the setup of the list of media centers being displayed in the listview.
 *
 * @author  TeamSDK
 * @since   October 25 2015
 * @version November 3 2015
 */
public class MCDAdapter extends ArrayAdapter<MediaCenterDescriptor> {

    /**
     * The constructor for MCDAdapter used to setup the ListView.
     *
     * @param context - The current context.
     * @param resource - The resource ID for a layout file containing a list_view ot use when
     *                 instantiating views.
     * @param devices - The objects to represent in the ListView.
     *
     * @since   October 25 2015
     * @version November 3 2015
     */
    public MCDAdapter(Context context, int resource, List<MediaCenterDescriptor> devices) {

        super(context, resource, devices); // use the constructor for ArrayAdapter.

    } // end constructor

    /**
     * The following method is used to get a View that display the data at the specified position in
     * the data set.
     *
     * @param position - The position of the item within the adapter's data set of the item whose
     *                 view we want.
     * @param convertView - The old view to reuse, if possible.
     * @param parent - The parent that this view will eventually be attached to.
     * @return A View corresponding to the data at the specified position.
     *
     * @since   October 25 2015
     * @version November 3 2015
     */
    @SuppressLint({"SetTextI18n", "InflateParams"})
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView; // use the old view

        if (v == null) { // if the old view is not available
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.media_center_list_item, null); // inflate from a XML layout file.
        }

        MediaCenterDescriptor device = getItem(position); // get the MCDDescriptor at the current position.

        if (device != null) { // if our MCDDescriptor is valid
            // retrieve the TextView placeholders within media_center_list_item to display media center parameters
            TextView tt1 = (TextView) v.findViewById(R.id.media_center_alias);
            TextView tt2 = (TextView) v.findViewById(R.id.media_center_IP);
            TextView tt3 = (TextView) v.findViewById(R.id.media_center_date);

            if (tt1 != null) { // if our TextView is valid
                tt1.setText(device.getAlias()); // set the alias of the media center
            }

            if (tt2 != null) { // if our TextView is valid
                tt2.setText(device.getIpAddr()); // set the IP address of the media center
            }

            if (tt3 != null) { // if our TextView is valid
                tt3.setText(device.getPairDate().toString()); // set the date paired for the media center
            }
        }

        return v;

    } // end method

} // end class