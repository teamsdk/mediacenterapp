package com.example.android.mediacenter.Activities;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.TextView;
import com.example.android.mediacenter.Misc.MediaPlayerCodes;
import com.example.android.mediacenter.Misc.RequestMediaPlayerFunction;
import com.example.android.mediacenter.Misc.ResizableImageView;
import com.example.android.mediacenter.R;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Pattern;

/**
 * This is the video playback controls screen for the Video Library.
 *
 * @author TeamSDK
 * @since November 3 2015
 * @version December 6 2015
 */
public class VideoPlayback extends AppCompatActivity {

    private static final String DATA_STORAGE = "MyDataFile"; // store data required
    private static final String GET_VIDEO_PLAYING = "v";

    // Our playback commands and array offsets
    private static final int PLAY_PAUSE = 0;
    private static final int PREVIOUS_FILE = 1;
    private static final int NEXT_FILE = 2;
    private static final int TITLE_INDEX = 0;
    private static final int CATEGORY_INDEX = 1;
    private static final int YEAR_INDEX = 2;

    private String videoTitle;
    private String videoCategoryYear;
    private String mediaCenterAddress;

    private TextView playbackVideoTitle;
    private TextView playbackCategoryYear;

    private ImageButton buttonPlayPause;
    private ImageButton buttonNext;
    private ImageButton buttonPrevious;

    private Timer getCurrentPlaybackTimer;

    private RequestMediaPlayerFunction playbackFunction;

    /**
     * This is the method invoked during the creation of this activity.
     *
     * @param savedInstanceState - The saved state this activity will be able to restore itself to
     *                           under special circumstances.
     * @return NONE
     *
     * @since November 3 2015
     * @version November 28 2015
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState); // Try and create from saved state. If not, new state
        setContentView(R.layout.activity_video_playback); // load the layout

        Toolbar toolbar = (Toolbar) findViewById(R.id.video_playback_toolbar); // get toolbar
        // get the ResizeableImageView used to display the album art
        ResizableImageView videoArt = (ResizableImageView) findViewById(R.id.video_art);
        SharedPreferences settings = getSharedPreferences(DATA_STORAGE, 0);

        playbackVideoTitle = (TextView) findViewById(R.id.playback_video_title);
        playbackCategoryYear = (TextView) findViewById(R.id.playback_video_category_year);

        mediaCenterAddress = settings.getString("currentDeviceIP", "");
        playbackFunction = new RequestMediaPlayerFunction(mediaCenterAddress);

        buttonPlayPause = (ImageButton) findViewById(R.id.playpause_video);
        buttonPlayPause.setOnClickListener(ButtonPlayPauseClick);

        buttonNext = (ImageButton) findViewById(R.id.next_video);
        buttonNext.setOnClickListener(ButtonNextClick);

        buttonPrevious = (ImageButton) findViewById(R.id.previous_video);
        buttonPrevious.setOnClickListener(ButtonPreviousClick);

        setSupportActionBar(toolbar);

        // here we get the initial information to display on the screen
        RequestMediaPlayerFunction getVideoInfo = new RequestMediaPlayerFunction(mediaCenterAddress);
        getVideoInfo.sendCommandRequest(MediaPlayerCodes.CURRENT_PLAYBACK, GET_VIDEO_PLAYING);
        String[] transformedInformation = new String(getVideoInfo.receiveRequestedInformation()).split(Pattern.quote("$"));

        if (transformedInformation[TITLE_INDEX].equals("Nothing") &&
                transformedInformation[CATEGORY_INDEX].equals("Nothing") &&
                transformedInformation[YEAR_INDEX].equals("Nothing")) {
            disableAllButtons();
        }

        videoTitle = transformedInformation[TITLE_INDEX];
        videoCategoryYear = transformedInformation[CATEGORY_INDEX] + " - " + transformedInformation[YEAR_INDEX];

        // set the TextViews to display title, category, & release year
        playbackVideoTitle.setText(videoTitle);
        playbackCategoryYear.setText(videoCategoryYear);

        // set the ResizeableImageView to display the video art
        videoArt.setImageResource(R.mipmap.ic_default_video_art);

        callGetVideoInformationTask();

    } // end method

    /**
     * The following method handles what happens in this activity when it is stopped.
     *
     * @return NONE
     *
     * @since December 3 2015
     * @version December 6 2015
     */
    @Override
    public void onStop() {

        super.onStop();
        playbackFunction.closeConnection();
        getCurrentPlaybackTimer.cancel();
        getCurrentPlaybackTimer.purge();

    } // end method

    /**
     * The following method enables the Previous and Next buttons.
     *
     * @return NONE
     *
     * @since November 27 2015
     * @version November 27 2015
     */
    private void enablePreviousAndNextButtons() {

        buttonNext.setEnabled(true);
        buttonPrevious.setEnabled(true);

    } // end method

    /**
     * The following method disables the Previous and Next buttons.
     *
     * @return NONE
     *
     * @since November 27 2015
     * @version November 27 2015
     */
    private void disablePreviousAndNextButtons() {


        buttonNext.setEnabled(false);
        buttonPrevious.setEnabled(false);

    } // end method

    /**
     * The following method disables all of the playback buttons.
     *
     * @return NONE
     *
     * @since November 27 2015
     * @version November 27 2015
     */
    private void disableAllButtons() {

        buttonPlayPause.setVisibility(View.GONE);
        buttonNext.setVisibility(View.GONE);
        buttonPrevious.setVisibility(View.GONE);

    } // end method

    /**
     * The following is the OnClickListener for the Play/Pause button
     *
     * @since November 27 2015
     * @version November 27 2015
     */
    private View.OnClickListener ButtonPlayPauseClick = new View.OnClickListener() {

        /**
         * The following method determines the action when the 'Play/Pause' button is clicked.
         *
         * @param view - the view that was clicked
         * @return NONE
         *
         * @since November 27 2015
         * @version November 27 2015
         */
        @Override
        public void onClick(View view) {

            playbackFunction.sendCommandRequest(MediaPlayerCodes.PLAYBACK_COMMAND, PLAY_PAUSE);

        } // end method

    }; // end anonymous class

    /**
     * The following is the OnClickListener for the Next button
     *
     * @since November 27 2015
     * @version November 27 2015
     */
    private OnClickListener ButtonNextClick = new OnClickListener() {

        /**
         * The following method determines the action when the 'Next' button is clicked.
         *
         * @param view - the view that was clicked
         * @return NONE
         *
         * @since November 27 2015
         * @version November 27 2015
         */
        @Override
        public void onClick(View view) {

            disablePreviousAndNextButtons();
            playbackFunction.sendCommandRequest(MediaPlayerCodes.PLAYLIST_COMMAND, NEXT_FILE);
            PlaybackCountDownTimer.start();

        } // end method

    }; // end anonymous class

    /**
     * The following is the OnClickListener for the Previous button
     *
     * @since November 27 2015
     * @version November 27 2015
     */
    private OnClickListener ButtonPreviousClick = new OnClickListener() {

        /**
         * The following method determines the action when the 'Previous' button is clicked.
         *
         * @param view - the view that was clicked
         * @return NONE
         *
         * @since November 27 2015
         * @version November 27 2015
         */
        @Override
        public void onClick(View view) {

            disablePreviousAndNextButtons();
            playbackFunction.sendCommandRequest(MediaPlayerCodes.PLAYLIST_COMMAND, PREVIOUS_FILE);
            PlaybackCountDownTimer.start();

        } // end method

    }; // end anonymous class

    /**
     * The following is the timer which will run and when finish will re enable the Next and
     * Previous button when complete to give the user the functionality back.
     *
     * @since November 27 2015
     * @version November 27 2015
     */
    private CountDownTimer PlaybackCountDownTimer =  new CountDownTimer(1000, 500) {

        /**
         * The method that is invoked when the timer finishes
         *
         * @return NONE
         *
         * @since November 27 2015
         * @version November 27 2015
         */
        @Override
        public void onFinish() {

            enablePreviousAndNextButtons();

        } // end method

        /**
         * The method that is invoked when the timer ticks
         *
         * @return NONE
         *
         * @since November 27 2015
         * @version November 27 2015
         */
        @Override
        public void onTick(long millisUntilFinished) {

        } // end method

    }.start(); // end anonymous class

    /**
     * The following method is run in the background to update our song information display. This is
     * done so the user can see what is currently being played on the device.
     *
     * @return NONE
     *
     * @since November 27 2015
     * @version November 27 2015
     */
    public void callGetVideoInformationTask() {
        final Handler handler = new Handler();
        getCurrentPlaybackTimer = new Timer();
        TimerTask doAsynchronousTask = new TimerTask() {

            /**
             * Our run method for our asynchronous task.
             *
             * @return NONE
             *
             * @since November 27 2015
             * @version November 27 2015
             */
            @Override
            public void run() {

                handler.post(new Runnable() {

                    /**
                     * Our run method for our handler.
                     *
                     * @return NONE
                     *
                     * @since November 27 2015
                     * @version November 27 2015
                     */
                    public void run() {
                        try {
                            new GetCurrentVideoPlayback().execute();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } // end method

                }); // end anonymous class

            } // end method

        }; // end anonymous class

        getCurrentPlaybackTimer.schedule(doAsynchronousTask, 0, 1000); // execute in every 1000 ms

    } // end method

    /**
     * This is contains the task to be run when a pull down for refresh is done.
     *
     * @author TeamSDK
     * @since November 27 2015
     * @version November 28 2015
     */
    private class GetCurrentVideoPlayback extends AsyncTask<Void, Void, Boolean> {

        /**
         * Following defines our background task to be done for the refresh and when entering
         * the fragment.
         *
         * @param params - the parameters of the task
         * @return A result.
         *
         * @since November 27 2015
         * @version November 28 2015
         */
        @Override
        protected Boolean doInBackground(Void... params) {

            Boolean disableButtons = false;
            RequestMediaPlayerFunction getVideoInfo = new RequestMediaPlayerFunction(mediaCenterAddress);
            getVideoInfo.sendCommandRequest(MediaPlayerCodes.CURRENT_PLAYBACK, GET_VIDEO_PLAYING);
            String[] transformedInformation = new String(getVideoInfo.receiveRequestedInformation()).split(Pattern.quote("$"));

            if (transformedInformation[TITLE_INDEX].equals("Nothing") &&
                    transformedInformation[CATEGORY_INDEX].equals("Nothing") &&
                    transformedInformation[YEAR_INDEX].equals("Nothing")) {
                disableButtons = true;
            }

            videoTitle = transformedInformation[TITLE_INDEX];
            videoCategoryYear = transformedInformation[CATEGORY_INDEX] + " - " + transformedInformation[YEAR_INDEX];

            return disableButtons;

        } // end method

        /**
         * Runs on the UI thread after doInBackground finishes its task.
         *
         * @param result - the result of the operations computed by doInBackground
         * @return NONE
         *
         * @since November 16 2015
         * @version November 19 2015
         */
        @Override
        protected void onPostExecute(Boolean result) {

            // set the TextViews to display title, & artist & album
            playbackVideoTitle.setText(videoTitle);
            playbackCategoryYear.setText(videoCategoryYear);

            if (result) {
                disableAllButtons();
            }

            super.onPostExecute(result);

        } // end method

    } // end class

} // end class