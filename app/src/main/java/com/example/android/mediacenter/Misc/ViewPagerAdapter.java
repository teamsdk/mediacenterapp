package com.example.android.mediacenter.Misc;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Our implementation of the FragmentPagerAdapter. Used to display a fragment in each Pager page.
 *
 * @author TeamSDK
 * @since November 19 2015
 * @version November 19 2015
 */
public class ViewPagerAdapter extends FragmentPagerAdapter {

    private List<Fragment> mFragmentList = new ArrayList<>(); // List to hold our fragments
    private List<String> mFragmentTitleList = new ArrayList<>(); // List to hold our fragment titles

    /**
     * Constructor to setup of ViewPagerAdapter
     *
     * @param manager - the FragmentManager of our activity
     *
     * @since November 19 2015
     * @version November 19 2015
     */
    public ViewPagerAdapter(FragmentManager manager) {

        super(manager);

    } // end method

    /**
     * Getter method to retrieve a fragment at a certain position.
     *
     * @param position - the position to retrieve the Fragment from
     * @return The Fragment at the specified location.
     *
     * @since November 19 2015
     * @version November 19 2015
     */
    @Override
    public Fragment getItem(int position) {

        return mFragmentList.get(position);

    } // end method

    /**
     * Retrieve the amount of Fragments we have stored.
     *
     * @return The number of Fragments that are stored
     *
     * @since November 19 2015
     * @version November 19 2015
     */
    @Override
    public int getCount() {

        return mFragmentList.size();

    } // end method

    /**
     * Add the Fragment and its corresponding title to the ViewPagerAdapter.
     *
     * @param fragment - the Fragment to add
     * @param title - the title of the Fragment
     * @return NONE
     *
     * @since November 19 2015
     * @version November 19 2015
     */
    public void addFragment(Fragment fragment, String title) {

        mFragmentList.add(fragment);
        mFragmentTitleList.add(title);

    } // end method

    /**
     * Getter method to retrieve the title of the Fragment
     *
     * @param position - the position to retrieve the title for
     * @return The title of the Fragment at the specified position
     *
     * @since November 19 2015
     * @version November 19 2015
     */
    @Override
    public String getPageTitle(int position) {

        return mFragmentTitleList.get(position);

    } // end method

} // end class