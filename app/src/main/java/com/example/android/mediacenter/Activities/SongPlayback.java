package com.example.android.mediacenter.Activities;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.TextView;
import com.example.android.mediacenter.Misc.MediaPlayerCodes;
import com.example.android.mediacenter.Misc.RequestMediaPlayerFunction;
import com.example.android.mediacenter.Misc.ResizableImageView;
import com.example.android.mediacenter.R;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Pattern;

/**
 * This is the song playback controls screen for the Music Library.
 *
 * @author  TeamSDK
 * @since   October 26 2015
 * @version December 6 2015
 */
public class SongPlayback extends AppCompatActivity {

    private static final String DATA_STORAGE = "MyDataFile"; // store data required
    private static final String GET_SONG_PLAYING = "s";

    // Our playback commands and array offsets
    private static final int PLAY_PAUSE = 0;
    private static final int PREVIOUS_FILE = 1;
    private static final int NEXT_FILE = 2;
    private static final int REPEAT_ON = 0;
    private static final int REPEAT_OFF = 1;
    private static final int SHUFFLE_ON = 2;
    private static final int SHUFFLE_OFF = 3;
    private static final int TITLE_INDEX = 0;
    private static final int ARTIST_INDEX = 1;
    private static final int ALBUM_INDEX = 2;
    private static final int SHUFFLE_INDEX = 3;
    private static final int REPEAT_INDEX = 4;

    private boolean shuffle;
    private boolean repeat;

    private String songTitle;
    private String songArtistAlbum;
    private String mediaCenterAddress;

    private TextView playbackSongTitle;
    private TextView playbackArtistAlbum;

    private ImageButton buttonPlayPause;
    private ImageButton buttonShuffle;
    private ImageButton buttonRepeat;
    private ImageButton buttonNext;
    private ImageButton buttonPrevious;

    private Timer getCurrentPlaybackTimer;

    private RequestMediaPlayerFunction playbackFunction;

    /**
     * This is the method invoked during the creation of this activity.
     *
     * @param savedInstanceState - The saved state this activity will be able to restore itself to
     *                           under special circumstances.
     * @return NONE
     *
     * @since October 26 2015
     * @version December 3 2015
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState); // Try and create from saved state. If not, new state
        setContentView(R.layout.activity_song_playback); // load the layout

        Toolbar toolbar = (Toolbar) findViewById(R.id.song_playback_toolbar); // get toolbar
        // get the ResizeableImageView used to display the album art
        ResizableImageView albumArt = (ResizableImageView) findViewById(R.id.album_art);
        SharedPreferences settings = getSharedPreferences(DATA_STORAGE, 0);

        playbackSongTitle = (TextView) findViewById(R.id.playback_song_title);
        playbackArtistAlbum = (TextView) findViewById(R.id.playback_artist_album);

        mediaCenterAddress = settings.getString("currentDeviceIP", "");
        shuffle = settings.getBoolean("shuffleState", false);
        repeat = settings.getBoolean("repeatState", false);

        playbackFunction = new RequestMediaPlayerFunction(mediaCenterAddress);

        buttonPlayPause = (ImageButton) findViewById(R.id.playpause_song);
        buttonPlayPause.setOnClickListener(ButtonPlayPauseClick);

        buttonShuffle = (ImageButton) findViewById(R.id.shuffle_song);
        buttonShuffle.setOnClickListener(ButtonShuffleClick);

        buttonRepeat = (ImageButton) findViewById(R.id.repeat_song);
        buttonRepeat.setOnClickListener(ButtonRepeatClick);

        buttonNext = (ImageButton) findViewById(R.id.next_song);
        buttonNext.setOnClickListener(ButtonNextClick);

        buttonPrevious = (ImageButton) findViewById(R.id.previous_song);
        buttonPrevious.setOnClickListener(ButtonPreviousClick);

        setSupportActionBar(toolbar);

        // here we get the initial information to display on the screen
        RequestMediaPlayerFunction getSongInfo = new RequestMediaPlayerFunction(mediaCenterAddress);
        getSongInfo.sendCommandRequest(MediaPlayerCodes.CURRENT_PLAYBACK, GET_SONG_PLAYING);
        String[] transformedInformation = new String(getSongInfo.receiveRequestedInformation()).split(Pattern.quote("$"));

        if (transformedInformation[TITLE_INDEX].equals("Nothing") &&
                transformedInformation[ARTIST_INDEX].equals("Nothing") &&
                transformedInformation[ALBUM_INDEX].equals("Nothing")) {
            disableAllButtons();
        }

        songTitle = transformedInformation[TITLE_INDEX];
        songArtistAlbum = transformedInformation[ARTIST_INDEX] + " - " + transformedInformation[ALBUM_INDEX];
        shuffle = Boolean.parseBoolean(transformedInformation[SHUFFLE_INDEX]);
        repeat = Boolean.parseBoolean(transformedInformation[REPEAT_INDEX]);

        buttonRepeat.setImageResource((repeat) ? R.drawable.ic_repeat_on : R.drawable.ic_repeat_off);
        buttonShuffle.setImageResource((shuffle) ? R.drawable.ic_shuffle_on : R.drawable.ic_shuffle_off);

        // set the TextViews to display title, artist, & album
        playbackSongTitle.setText(songTitle);
        playbackArtistAlbum.setText(songArtistAlbum);

        // set the ResizeableImageView to display the album art
        albumArt.setImageResource(R.mipmap.ic_default_album_art);

        callGetSongInformationTask();

    } // end method

    /**
     * The following method handles what happens in this activity when it is stopped.
     *
     * @return NONE
     *
     * @since December 3 2015
     * @version December 6 2015
     */
    @Override
    public void onStop() {

        super.onStop();
        playbackFunction.closeConnection();
        getCurrentPlaybackTimer.cancel();
        getCurrentPlaybackTimer.purge();

    } // end method

    /**
     * The following method enables the Previous and Next buttons.
     *
     * @return NONE
     *
     * @since November 27 2015
     * @version November 27 2015
     */
    private void enablePreviousAndNextButtons() {

        buttonNext.setEnabled(true);
        buttonPrevious.setEnabled(true);

    } // end method

    /**
     * The following method disables the Previous and Next buttons.
     *
     * @return NONE
     *
     * @since November 27 2015
     * @version November 27 2015
     */
    private void disablePreviousAndNextButtons() {

        buttonNext.setEnabled(false);
        buttonPrevious.setEnabled(false);

    } // end method

    /**
     * The following method disables all of the playback buttons.
     *
     * @return NONE
     *
     * @since November 27 2015
     * @version November 27 2015
     */
    private void disableAllButtons() {

        buttonPlayPause.setVisibility(View.GONE);
        buttonShuffle.setVisibility(View.GONE);
        buttonRepeat.setVisibility(View.GONE);
        buttonNext.setVisibility(View.GONE);
        buttonPrevious.setVisibility(View.GONE);

    } // end method

    /**
     * The following is the OnClickListener for the Play/Pause button
     *
     * @since November 27 2015
     * @version November 27 2015
     */
    private OnClickListener ButtonPlayPauseClick = new OnClickListener() {

        /**
         * The following method determines the action when the 'Play/Pause' button is clicked.
         *
         * @param view - the view that was clicked
         * @return NONE
         *
         * @since November 27 2015
         * @version November 27 2015
         */
        @Override
        public void onClick(View view) {

            playbackFunction.sendCommandRequest(MediaPlayerCodes.PLAYBACK_COMMAND, PLAY_PAUSE);

        } // end method

    }; // end anonymous class

    /**
     * The following is the OnClickListener for the Shuffle button
     *
     * @since November 27 2015
     * @version November 27 2015
     */
    private OnClickListener ButtonShuffleClick = new OnClickListener() {

        /**
         * The following method determines the action when the 'Shuffle' button is clicked.
         *
         * @param view - the view that was clicked
         * @return NONE
         *
         * @since November 27 2015
         * @version November 27 2015
         */
        @Override
        public void onClick(View view) {

            if (shuffle) {
                playbackFunction.sendCommandRequest(MediaPlayerCodes.PLAYLIST_SETTING, SHUFFLE_OFF);
                buttonShuffle.setImageResource(R.drawable.ic_shuffle_off);
                shuffle = false;
            }
            else {
                playbackFunction.sendCommandRequest(MediaPlayerCodes.PLAYLIST_SETTING, SHUFFLE_ON);
                buttonShuffle.setImageResource(R.drawable.ic_shuffle_on);
                shuffle = true;
            }

        } // end method

    }; // end anonymous class

    /**
     * The following is the OnClickListener for the Repeat button
     *
     * @since November 27 2015
     * @version November 27 2015
     */
    private OnClickListener ButtonRepeatClick = new OnClickListener() {

        /**
         * The following method determines the action when the 'Repeat' button is clicked.
         *
         * @param view - the view that was clicked
         * @return NONE
         *
         * @since November 27 2015
         * @version November 27 2015
         */
        @Override
        public void onClick(View view) {

            if (repeat) {
                playbackFunction.sendCommandRequest(MediaPlayerCodes.PLAYLIST_SETTING, REPEAT_OFF);
                buttonRepeat.setImageResource(R.drawable.ic_repeat_off);
                repeat = false;
            }
            else {
                playbackFunction.sendCommandRequest(MediaPlayerCodes.PLAYLIST_SETTING, REPEAT_ON);
                buttonRepeat.setImageResource(R.drawable.ic_repeat_on);
                repeat = true;
            }

        } // end method

    }; // end anonymous class

    /**
     * The following is the OnClickListener for the Next button
     *
     * @since November 27 2015
     * @version November 27 2015
     */
    private OnClickListener ButtonNextClick = new OnClickListener() {

        /**
         * The following method determines the action when the 'Next' button is clicked.
         *
         * @param view - the view that was clicked
         * @return NONE
         *
         * @since November 27 2015
         * @version November 27 2015
         */
        @Override
        public void onClick(View view) {

            disablePreviousAndNextButtons();
            playbackFunction.sendCommandRequest(MediaPlayerCodes.PLAYLIST_COMMAND, NEXT_FILE);
            PlaybackCountDownTimer.start();

        } // end method

    }; // end anonymous class

    /**
     * The following is the OnClickListener for the Previous button
     *
     * @since November 27 2015
     * @version November 27 2015
     */
    private OnClickListener ButtonPreviousClick = new OnClickListener() {

        /**
         * The following method determines the action when the 'Previous' button is clicked.
         *
         * @param view - the view that was clicked
         * @return NONE
         *
         * @since November 27 2015
         * @version November 27 2015
         */
        @Override
        public void onClick(View view) {

            disablePreviousAndNextButtons();
            playbackFunction.sendCommandRequest(MediaPlayerCodes.PLAYLIST_COMMAND, PREVIOUS_FILE);
            PlaybackCountDownTimer.start();

        } // end method

    }; // end anonymous class

    /**
     * The following is the timer which will run and when finish will re enable the Next and
     * Previous button when complete to give the user the functionality back.
     *
     * @since November 27 2015
     * @version November 27 2015
     */
    private CountDownTimer PlaybackCountDownTimer =  new CountDownTimer(1000, 500) {

        /**
         * The method that is invoked when the timer finishes
         *
         * @return NONE
         *
         * @since November 27 2015
         * @version November 27 2015
         */
        @Override
        public void onFinish() {

            enablePreviousAndNextButtons();

        } // end method

        /**
         * The method that is invoked when the timer ticks
         *
         * @return NONE
         *
         * @since November 27 2015
         * @version November 27 2015
         */
        @Override
        public void onTick(long millisUntilFinished) {

        } // end method

    }.start(); // end anonymous class

    /**
     * The following method is run in the background to update our song information display. This is
     * done so the user can see what is currently being played on the device.
     *
     * @return NONE
     *
     * @since November 27 2015
     * @version November 27 2015
     */
    private void callGetSongInformationTask() {

        final Handler handler = new Handler();
        getCurrentPlaybackTimer = new Timer();
        TimerTask doAsynchronousTask = new TimerTask() {

            /**
             * Our run method for our asynchronous task.
             *
             * @return NONE
             *
             * @since November 27 2015
             * @version November 27 2015
             */
            @Override
            public void run() {

                handler.post(new Runnable() {

                    /**
                     * Our run method for our handler.
                     *
                     * @return NONE
                     *
                     * @since November 27 2015
                     * @version November 27 2015
                     */
                    public void run() {

                        try {
                            new GetCurrentSongPlayback().execute();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } // end method

                }); // end anonymous class

            } // end method

        }; // end anonymous class

        getCurrentPlaybackTimer.schedule(doAsynchronousTask, 0, 1000); // execute in every 1000 ms

    } // end method

    /**
     * This is contains the task to be run to retrieve the current playback
     *
     * @author TeamSDK
     * @since November 27 2015
     * @version November 28 2015
     */
    private class GetCurrentSongPlayback extends AsyncTask<Void, Void, Boolean> {

        /**
         * Following defines our background task to be done for getting the current playback for the
         * song now playing menu.
         *
         * @param params - the parameters of the task
         * @return A result.
         *
         * @since November 27 2015
         * @version November 28 2015
         */
        @Override
        protected Boolean doInBackground(Void... params) {

            Boolean disableButtons = false;
            RequestMediaPlayerFunction getSongInfo = new RequestMediaPlayerFunction(mediaCenterAddress);
            getSongInfo.sendCommandRequest(MediaPlayerCodes.CURRENT_PLAYBACK, GET_SONG_PLAYING);
            String[] transformedInformation = new String(getSongInfo.receiveRequestedInformation()).split(Pattern.quote("$"));

            if (transformedInformation[TITLE_INDEX].equals("Nothing") &&
                    transformedInformation[ARTIST_INDEX].equals("Nothing") &&
                    transformedInformation[ALBUM_INDEX].equals("Nothing")) {
                disableButtons = true;
            }

            songTitle = transformedInformation[TITLE_INDEX];
            songArtistAlbum = transformedInformation[ARTIST_INDEX] + " - " + transformedInformation[ALBUM_INDEX];

            shuffle = Boolean.parseBoolean(transformedInformation[SHUFFLE_INDEX]);
            repeat = Boolean.parseBoolean(transformedInformation[REPEAT_INDEX]);
            Log.e("SongPlayback", transformedInformation[REPEAT_INDEX].toLowerCase());

            return disableButtons;

        } // end method

        /**
         * Runs on the UI thread after doInBackground finishes its task.
         *
         * @param result - the result of the operations computed by doInBackground
         * @return NONE
         *
         * @since November 27 2015
         * @version November 28 2015
         */
        @Override
        protected void onPostExecute(Boolean result) {

            // set the TextViews to display title, & artist & album
            playbackSongTitle.setText(songTitle);
            playbackArtistAlbum.setText(songArtistAlbum);

            buttonRepeat.setImageResource((repeat) ? R.drawable.ic_repeat_on : R.drawable.ic_repeat_off);
            buttonShuffle.setImageResource((shuffle) ? R.drawable.ic_shuffle_on : R.drawable.ic_shuffle_off);

            if (result) {
                disableAllButtons();
            }

            super.onPostExecute(result);

        } // end method

    } // end class

} // end class