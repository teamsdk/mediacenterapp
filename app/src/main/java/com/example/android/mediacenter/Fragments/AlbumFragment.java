package com.example.android.mediacenter.Fragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import com.example.android.mediacenter.Activities.AlbumActivity;
import com.example.android.mediacenter.Adapters.AlbumAdapter;
import shared.AlbumDescriptor;
import com.example.android.mediacenter.Misc.QueryCodes;
import com.example.android.mediacenter.Misc.RequestQuery;
import com.example.android.mediacenter.R;
import java.io.ObjectInputStream;
import java.util.LinkedList;

/**
 * The fragment to use when loading a albums view in the music library.
 *
 * @author TeamSDK
 * @since November 15 2015
 * @version November 28 2015
 */
public class AlbumFragment extends Fragment
                           implements SwipeRefreshLayout.OnRefreshListener {

    private static final String DATA_STORAGE = "MyDataFile"; // store data require
    private static final int ALBUM_PORT = 4003;

    private SwipeRefreshLayout swipeLayout; // our refreshing swipe layout we are using
    private AlbumAdapter albumAdapter;
    private LinkedList<AlbumDescriptor> listOfAlbums = new LinkedList<>(); // our albums
    private String mediaCenterAddress;
    private QueryCodes query; // the type of query this fragment will issue
    private Boolean needsID; // if a database id field is required in this fragment
    private int id; // the database id if required

    /**
     * Called to have the fragment instantiate its user interface view.
     *
     * @param inflater - the layoutinflater object that can be used to inflate any views in the fragment
     * @param container - this is the parent view that the fragment's UI should be attached to
     * @param savedInstanceState - the previous state to re-construct this fragment from.
     * @return The view for the fragment's UI
     *
     * @since November 15 2015
     * @version November 28 2015
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_album, container, false); // get the view
        SharedPreferences settings = this.getActivity().getSharedPreferences(DATA_STORAGE, 0);
        Bundle bundle = getArguments();

        ListView listView = (ListView) view.findViewById(R.id.list_music_albums);

        swipeLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_albums_container);
        swipeLayout.setOnRefreshListener(this);

        // Restore currently selected device
        mediaCenterAddress = settings.getString("currentDeviceIP", "");

        // retrieve information needed in this fragment
        query = QueryCodes.values()[bundle.getInt("query")];
        needsID = bundle.getBoolean("needsID");

        if (needsID) {
            id = bundle.getInt("artistID");
        }

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            /**
             * The following method handles the clicking of albums in the list of albums.
             *
             * @param parent - The AdapterView where the click happened.
             * @param view - The view within the AdapterView that was clicked.
             * @param position - The position of the view in the adapter.
             * @param id - The row id of the item that was clicked.
             * @return NONE
             *
             * @since November 15 2015
             * @version November 19 2015
             */
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                AlbumDescriptor album = (AlbumDescriptor) parent.getItemAtPosition(position);
                Intent intent = new Intent(getActivity(), AlbumActivity.class); // set launch of activity
                intent.putExtra("albumName", album.getAlbumTitle());
                intent.putExtra("albumID", album.getId());
                startActivity(intent); // start the new activity

            } // end method

        } // end anonymous class
        );

        albumAdapter = new AlbumAdapter(getActivity().getBaseContext(), R.layout.album_list_item, listOfAlbums);
        listView.setAdapter(albumAdapter);

        new GetAlbumsList().execute();

        return view;

    } // end method

    /**
     * This is the handler for pulling down for refresh.
     *
     * @return NONE
     *
     * @since November 15 2015
     * @version November 28 2015
     */
    @Override
    public void onRefresh() {

        listOfAlbums.clear();
        albumAdapter.notifyDataSetChanged();
        new GetAlbumsList().execute();

    } // end method

    /**
     * This is contains the task to be run when a pull down for refresh is done and when entering
     * the fragment.
     *
     * @author TeamSDK
     * @since November 16 2015
     * @version November 28 2015
     */
    private class GetAlbumsList extends AsyncTask<Void, Void, Void> {

        /**
         * Following defines our background task to be done.
         *
         * @param params - the parameters of the task
         * @return A result.
         *
         * @since November 16 2015
         * @version November 28 2015
         */
        @SuppressWarnings("unchecked")
        @Override
        protected Void doInBackground(Void... params) {


            RequestQuery albums = new RequestQuery(mediaCenterAddress, ALBUM_PORT);

            if (needsID) { // query the media center for albums for an artist
                albums.sendQueryRequest(query, id);
            }
            else { // query the media center for all albums
                albums.sendQueryRequest(query);
            }

            try {
                ObjectInputStream iStream = albums.receiveRequestedInformation();
                listOfAlbums.addAll((LinkedList<AlbumDescriptor>) iStream.readObject());
                iStream.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;

        } // end method

        /**
         * Runs on the UI thread after doInBackground finishes its task.
         *
         * @param result - the result of the operations computed by doInBackground
         * @return NONE
         *
         * @since November 16 2015
         * @version November 28 2015
         */
        @Override
        protected void onPostExecute(Void result) {

            albumAdapter.notifyDataSetChanged();
            swipeLayout.setRefreshing(false);
            super.onPostExecute(result);

        } // end method

    } // end class

} // end class