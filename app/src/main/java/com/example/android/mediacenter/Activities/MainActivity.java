package com.example.android.mediacenter.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;
import com.example.android.mediacenter.R;

/**
 * This is the Main/Home menu for the Media Center. Here the user can view which media center they
 * are currently connected to. The user can navigate to different menus depending on their desired
 * usage from here.
 *
 * @author  TeamSDK
 * @since   October 21 2015
 * @version October 30 2015
 */
public class MainActivity extends AppCompatActivity
                          implements NavigationView.OnNavigationItemSelectedListener {

    private static final String DATA_STORAGE = "MyDataFile";

    /**
     * This is the method invoked during the creation of this activity.
     *
     * @param savedInstanceState - The saved state this activity will be able to restore itself to
     *                           under special circumstances.
     * @return NONE
     *
     * @since October 21 2015
     * @version November 13 2015
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState); // try and create from saved state. If not, new state
        setContentView(R.layout.activity_main); // load the layout

        Toolbar toolbar = (Toolbar) findViewById(R.id.main_toolbar); // get toolbar
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.main_menu_drawer_layout);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);

        // Restore the current device that the app is hooked up to
        SharedPreferences settings = getSharedPreferences(DATA_STORAGE, 0);
        String currentDevice = settings.getString("currentDevice", "");
        TextView current = (TextView) findViewById(R.id.text_alias);
        current.setText(currentDevice);

        setSupportActionBar(toolbar); // set toolbar actions
        /*
         * Setup the drawer within this activity and the navigation. Navigation is controlled via
         * the navigation item selected listener.
         */
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

    } // end method

    /**
     * The following method handles what happens in this menu when the back button is pressed.
     *
     * @return NONE
     *
     * @since October 21 2015
     * @version October 30 2015
     */
    @Override
    public void onBackPressed() {

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.main_menu_drawer_layout);

        if (drawer.isDrawerOpen(GravityCompat.START)) { // if the navigation view is open, close it
            drawer.closeDrawer(GravityCompat.START);
        }
        else { // go back to the main menu
            super.onBackPressed();
        }

    } // end method

    /**
     * The following method is the listener for the navigation view and determines the action
     * based on which button is hit in the drawer.
     *
     * @param item - The item that was selected in the navigation view.
     * @return true
     *
     * @since October 21 2015
     * @version October 30 2015
     */
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        int id = item.getItemId(); // retrieve the ID of the navigation view button pressed
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.main_menu_drawer_layout);

        if (id == R.id.nav_music) { // if music library button pressed
            Intent intent = new Intent(this, MusicLibrary.class); // set launch of activity
            startActivity(intent); // start the new activity
        }
        else if (id == R.id.nav_video) { // if video library button pressed
            Intent intent = new Intent(this, VideoLibrary.class); // set launch of activity
            startActivity(intent); // start the new activity
        }
        else if (id == R.id.nav_config) { // if configuration button pressed
            Intent intent = new Intent(this, ConfigurationMenu.class); // set launch of activity
            startActivity(intent); // start the new activity
        }
        else if (id == R.id.nav_pair) { // if pairing button pressed
            Intent intent = new Intent(this, PairingMenu.class); // set launch of activity
            startActivity(intent); // start the new activity
        }

        drawer.closeDrawer(GravityCompat.START); // close the view after navigation
        return true;

    } // end method

} // end class