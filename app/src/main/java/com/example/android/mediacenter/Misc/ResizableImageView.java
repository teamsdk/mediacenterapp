package com.example.android.mediacenter.Misc;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * The following class provides an ImageView which can be resized to wrap the image properly.
 *
 * @author  TeamSDK
 * @since   October 23 2015
 * @version October 30 2015
 */
public class ResizableImageView extends ImageView {

    /**
     * The constructor used to initialize the ResizeableImageView.
     *
     * @param context - The current context.
     * @param attrs - The attributes to be assigned the the ResizeableImageView.
     *
     * @since   October 23 2015
     * @version October 30 2015
     */
    public ResizableImageView(Context context, AttributeSet attrs) {

        super(context, attrs); // use the constructor for ImageView.

    } // end constructor

    /**
     * Measure the view and its content to determine the measure width and measure height.
     *
     * @param widthMeasureSpec - horizontal space requirements as imposed by the parent.
     * @param heightMeasureSpec - vertical space requirements as imposed by the parent.
     * @return NONE
     *
     * @since   October 23 2015
     * @version October 30 2015
     */
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        Drawable d = getDrawable();

        if (d != null) { // if we have drawable
            // ceil not round - avoid thin vertical gaps along the left/right edges
            int width = MeasureSpec.getSize(widthMeasureSpec);
            int height = (int) Math.ceil((float) width * (float) d.getIntrinsicHeight() / (float) d.getIntrinsicWidth());
            setMeasuredDimension(width, height);
        }
        else {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        }

    } // end method

} // end class